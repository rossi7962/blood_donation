import React, { useEffect, useState, useCallback } from "react";
import api from "../../config/api";
import Router from "next/router";
import { connect } from 'react-redux';
import PageLayout from "../../src/components/PageLayout.js"
import StringUtil from '../../src/utils/StringUtils'
import { Page, Filters, Layout, Pagination, Card, ResourceList, Avatar, ResourceItem, TextStyle, Modal, TextField, Select } from "@shopify/polaris"
import { HeartMajorMonotone, ArchiveMajorMonotone, OrdersMajorMonotone, ArrowLeftMinor, PlusMinor } from "@shopify/polaris-icons";
import Constants from "../../src/utils/Constants.js";

let offset = 0
let limit = Constants.LIMIT

function BloodOrder(props) {
    const [active, setActive] = useState(false);

      //display orders
      const resourceName = {
        singular: 'order',
        plural: 'orders',
    };
    const [bloodOrders, setBloodOrders] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [rawBloodOrders, setRawBloodOrders] = useState([]);
    const [selectedItems, setSelectedItems] = useState([]);

    const getOrders = () => {
        setIsLoading(true)
        let payload = {
            offset: offset,
            limit: limit
        }
        api.getSentBloodOrders(payload, (isSuccess, response, error) => {
            if (isSuccess) {
                if (response.data.data.length === 0) {
                    setBloodOrders([])
                    setRawBloodOrders([])
                    setIsLoading(false)
                }
                else {
                    // CHANGE FROM UNIX TIME TO DATETIME
                    for (let i = 0; i < response.data.data.length; i++) {
                        let order_date = response.data.data[i].order_date
                        response.data.data[i].order_date = StringUtil.getDate(order_date)
                        response.data.data[i].id = response.data.data[i].order_id
                    }
                    let tempOrders = JSON.parse(JSON.stringify(response.data.data))
                    let sortedOrders = sortBloodOrders(sortValue, tempOrders)
                    setBloodOrders(handleQueryValueChange(queryValue, sortedOrders));

                    setRawBloodOrders(sortedOrders);
                    console.log(bloodOrders)
                    setIsLoading(false)
                }
            } else {
                console.log("ERROR GETTING BLOOD ORDERS: ", error)
            }
        })
    }
    ////////////////////////////

   //sort
    const [sortValue, setSortValue] = useState('DATE_DESC');
    const sortBloodOrders = (selected, array) => {
        function sortDateDesc(a, b) {
            return new Date(b.order_date) - new Date(a.order_date)
        }

        function sortDateAsc(a, b) {
            return new Date(a.order_date) - new Date(b.order_date)
        }
        let arr = JSON.parse(JSON.stringify(array));

        if (selected === "DATE_ASC") {
            arr.sort(sortDateAsc)
        } else if (selected === "DATE_DESC") {
            arr.sort(sortDateDesc)
        }
        return arr;
    }
    //////////////////////////////

    //search bar
    const handleTaggedWithRemove = useCallback(() => setTaggedWith(null), []);
    const handleQueryValueRemove = useCallback(() => setQueryValue(null), []);

    const filters = [
    ];
    const [queryValue, setQueryValue] = useState("");
    const handleClearAll = useCallback(() => {
        handleTaggedWithRemove();
        handleQueryValueRemove();
    }, [handleQueryValueRemove, handleTaggedWithRemove]);
    
    const handleQueryValueChange = (value, array) => {
        let filteredOrders = JSON.parse(JSON.stringify(array));
        filteredOrders = filteredOrders.filter((order) => {
            let blood_type = order.blood_type
            return blood_type.includes(value);
        })
        setQueryValue(value)
        console.log("Value: ", value)
        console.log("raw: ", rawBloodOrders)
        console.log("FILTERED ORDERS: ", filteredOrders)
        return filteredOrders
    }
    const filterControl = (
        <Filters
            queryValue={queryValue}
            filters={filters}
            onQueryChange={(queryVal) => {
                setBloodOrders(handleQueryValueChange(queryVal, rawBloodOrders))
            }}
            onQueryClear={handleQueryValueRemove}
            onClearAll={handleClearAll}
        >
        </Filters>
    );
    ///////////////////////////////

    useEffect(() => {
        getOrders();
    }, [])

    function renderItem(item) {
        const { order_id, order_date, amount, blood_type, status } = item;
        let variation = "positive"
        if (status === "unsent") variation = "negative"
        return (
            <ResourceItem
                id={order_id}
                url={null}
                accessibilityLabel={`View details for ${order_id}`}
            >
                <h3>
                    <TextStyle variation="strong">Order ID: {order_id}</TextStyle>
                </h3>
                <div>Date: {order_date}</div>
                <div>Amount: {amount}</div>
                <div>Blood Type: {blood_type}</div>
                <div>Status: {" "}
                    <TextStyle variation={variation}>
                        {status}
                    </TextStyle>
                </div>

            </ResourceItem>
        );
    }
    /////////////////////////


    return (
        <PageLayout userName={props.userName}
            userType={props.userType}
            title={"Managing Blood Orders"}
            expectedUserType={"hospital"}
            navigationArray={[
                {
                    label: 'Manage Blood Orders',
                    icon: OrdersMajorMonotone,
                    onClick: () => {
                        Router.push("/hospital/manage_blood_order").then(() => {
                            console.log("Route successfully from 'hospital/blood_order_status' to 'hospital/manage_blood_order'");
                        })
                    },
                },
                {
                    label: 'View Sent Orders Status',
                    icon: ArchiveMajorMonotone,
                    onClick: () => {
                        
                    },
                }
            ]}
        >
            <Page
                fullWidth
                title="View Sent Blood Orders Status"
                subtitle={bloodOrders.length ? "Check status of previously sent orders" : "No orders available"}
            >
                {/* display order list */}
                <Layout>
                    <Layout.Section>
                        <Card title="Blood Orders">
                            <ResourceList
                                resourceName={resourceName}
                                items={bloodOrders}
                                selectedItems={selectedItems}
                                onSelectionChange={(item) => {
                                    setSelectedItems(item);
                                    console.log(selectedItems);
                                }}
                                sortValue={sortValue}
                                sortOptions={[
                                    { label: 'Date (Descending)', value: 'DATE_DESC' },
                                    { label: 'Date (Ascending)', value: 'DATE_ASC' },
                                ]}
                                onSortChange={(selected) => {
                                    setSortValue(selected);
                                    setBloodOrders(sortBloodOrders(selected, bloodOrders));
                                    setRawBloodOrders(sortBloodOrders(selected, rawBloodOrders));
                                    console.log(`Sort option changed to ${selected}.`);
                                }}
                                renderItem={renderItem}
                                onSelectionChange={(item) => {
                                    setSelectedItems(item);
                                    console.log(selectedItems);
                                }}
                                filterControl={filterControl}
                                loading={isLoading}
                            />
                        </Card>
                    </Layout.Section>
                </Layout>

            </Page>
            
            {/* pagination */}
            <div className="outer">
                <div className="inner">
                    <Pagination
                        hasPrevious
                        onPrevious={() => {
                            if (offset > 0) {
                                offset -= Constants.LIMIT
                                getOrders()
                            }
                        }}
                        hasNext
                        onNext={() => {
                            if (rawBloodOrders.length !== 0) {
                                // MEANING THAT IT HAS NOT REACHED THE END OF THE LIST
                                if (rawBloodOrders.length === 3) {
                                    offset += Constants.LIMIT
                                    getOrders()
                                }
                            }
                        }}
                    />
                </div>
            </div>
        </PageLayout>
    )
}

export default connect(state => ({
    userName: state.LoginReducer.userName,
    email: state.LoginReducer.email,
    userType: state.LoginReducer.userType,
    userId: state.LoginReducer.userId
}))(BloodOrder);