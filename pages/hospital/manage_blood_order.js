import React, { useEffect, useState, useCallback } from "react";
import api from "../../config/api";
import Router from "next/router";
import { connect } from 'react-redux';
import PageLayout from "../../src/components/PageLayout.js"
import StringUtil from '../../src/utils/StringUtils'
import { Page, Stack, Checkbox, Pagination, Filters, Layout, FormLayout, Card, ResourceList, Avatar, ResourceItem, TextStyle, Modal, TextField, Select } from "@shopify/polaris"
import { ArchiveMajorMonotone, OrdersMajorMonotone, ArrowLeftMinor, PlusMinor } from "@shopify/polaris-icons";
import Constants from "../../src/utils/Constants.js";

let offset = 0
let limit = Constants.LIMIT

function BloodOrder(props) {
    const [active, setActive] = useState(false);

    //create order
    const options = [
        { label: 'A+', value: 'A+' },
        { label: 'A-', value: 'A-' },
        { label: 'B+', value: 'B+' },
        { label: 'B-', value: 'B-' },
        { label: 'O+', value: 'O+' },
        { label: 'O', value: 'O' },
        { label: 'AB+', value: 'AB+' },
        { label: 'AB-', value: 'AB-' },
    ];
    const handleModalChange = useCallback(() => setActive(!active), [active]);
    const [amount, setAmount] = useState('');
    const [selectedDates, setSelectedDates] = useState(new Date());
    const handleChange = useCallback(() => setActive(!active), [active]);
    const [bloodtype, setBloodType] = useState('A+');
    const handleAmountChange = useCallback((value) => {
        setAmount(value)
    }, []);
    
    const handleBloodTypeChange = useCallback((value) => {
        setBloodType(value)
    }, []);

    const handleClose = () => {
        handleModalChange();
        setAmount('');
        setSelectedDates(new Date());
    };
    const handleCreate = () => {
        setSelectedDates(new Date())
        let unixDate = (selectedDates.getTime() / 1000).toFixed(0)
        let payload = {
            date: unixDate,
            amount: amount,
            blood_type: bloodtype
        }
        console.log("manage_blood_order | handleCreate | payload: ", payload);
        api.createBloodOrders(payload, (isSuccess, response, error) => {
            if (isSuccess) {
                handleClose()
                getOrders()
            }
        })
        setAmount('');
        setSelectedDates(new Date())
    }
    //////////////////////////

    //display orders
    const resourceName = {
        singular: 'order',
        plural: 'orders',
    };
    const [bloodOrders, setBloodOrders] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [rawBloodOrders, setRawBloodOrders] = useState([]);
    const [selectedItems, setSelectedItems] = useState([]);

    const getOrders = () => {
        setIsLoading(true)
        let payload = {
            offset: offset,
            limit: limit
        }
        api.getUnsentBloodOrders(payload, (isSuccess, response, error) => {
            if (isSuccess) {
                if (response.data.data.length === 0) {
                    setBloodOrders([])
                    setRawBloodOrders([])
                    setIsLoading(false)
                }
                else {
                    // CHANGE FROM UNIX TIME TO DATETIME
                    for (let i = 0; i < response.data.data.length; i++) {
                        let order_date = response.data.data[i].order_date
                        response.data.data[i].order_date = StringUtil.getDate(order_date)
                        response.data.data[i].id = response.data.data[i].order_id
                    }
                    let tempOrders = JSON.parse(JSON.stringify(response.data.data))
                    let sortedOrders = sortBloodOrders(sortValue, tempOrders)
                    setBloodOrders(handleQueryValueChange(queryValue, sortedOrders));

                    setRawBloodOrders(sortedOrders);
                    console.log(bloodOrders)
                    setIsLoading(false)
                }
            } else {
                console.log("ERROR GETTING BLOOD ORDERS: ", error)
            }
        })
    }
    ////////////////////////////

    //sort
    const [sortValue, setSortValue] = useState('DATE_DESC');
    const sortBloodOrders = (selected, array) => {
        function sortDateDesc(a, b) {
            return new Date(b.order_date) - new Date(a.order_date)
        }

        function sortDateAsc(a, b) {
            return new Date(a.order_date) - new Date(b.order_date)
        }
        let arr = JSON.parse(JSON.stringify(array));

        if (selected === "DATE_ASC") {
            arr.sort(sortDateAsc)
        } else if (selected === "DATE_DESC") {
            arr.sort(sortDateDesc)
        }
        return arr;
    }
    //////////////////////////////

    //search bar
    const handleTaggedWithRemove = useCallback(() => setTaggedWith(null), []);
    const handleQueryValueRemove = useCallback(() => setQueryValue(null), []);

    const filters = [
    ];
    const [queryValue, setQueryValue] = useState("");
    const handleClearAll = useCallback(() => {
        handleTaggedWithRemove();
        handleQueryValueRemove();
    }, [handleQueryValueRemove, handleTaggedWithRemove]);
    
    const handleQueryValueChange = (value, array) => {
        let filteredOrders = JSON.parse(JSON.stringify(array));
        filteredOrders = filteredOrders.filter((order) => {
            let blood_type = order.blood_type
            return blood_type.includes(value);
        })
        setQueryValue(value)
        console.log("Value: ", value)
        console.log("raw: ", rawBloodOrders)
        console.log("FILTERED ORDERS: ", filteredOrders)
        return filteredOrders
    }
    const filterControl = (
        <Filters
            queryValue={queryValue}
            filters={filters}
            onQueryChange={(queryVal) => {
                setBloodOrders(handleQueryValueChange(queryVal, rawBloodOrders))
            }}
            onQueryClear={handleQueryValueRemove}
            onClearAll={handleClearAll}
        >
        </Filters>
    );
    ///////////////////////////////

    //edit order
    const [amountError, setamountError] = useState('');

    const [editOrderAmount, seteditOrderAmount] = useState('');
    const [editOrderBloodType, seteditOrderBloodType] = useState(''); 
    const [editOrderStatus, seteditOrderStatus] = useState(''); 
    const [editOrderID, seteditOrderID] = useState(''); 
    const [originalID, setOriginalID] = useState('');
    const [originalStatus, setOriginalStatus] = useState('');
    const [originalAmount, setOriginalAmount] = useState('');
    const [originalBloodType, setOriginalBloodType] = useState('');
    const [checked, setChecked] = useState(false);
    const handleCheckboxChange = useCallback((newChecked) => setChecked(newChecked), []);


    const [updateModalActive, setUpdateModalActive] = useState(false);
    const handleUpdateModalClose = useCallback(() => {
        setUpdateModalActive(!updateModalActive)
        resetError();
    }, [updateModalActive]);
    const checkError = (amount) => {
        if (StringUtil.isEmpty(amount)) setamountError("Order amount cannot be empty")
    }
    const resetError = () => {
        setamountError('');
    }

    const handleEditItem = useCallback((order_id) => {
        api.getBloodOrderWithID(order_id, (isSuccess, response, error) => {
            if (isSuccess) {
                seteditOrderID(response.data.data[0].order_id)
                seteditOrderAmount(response.data.data[0].amount)
                seteditOrderBloodType(response.data.data[0].blood_type)
                seteditOrderStatus(response.data.data[0].status)
                setOriginalID(response.data.data[0].order_id)
                setOriginalAmount(response.data.data[0].amount)
                setOriginalBloodType(response.data.data[0].blood_type)
                setOriginalStatus(response.data.data[0].status)
            } else {
                console.log("ERROR IN GETTING ONE BLOOD ORDER !", error)
            }
        })
        setUpdateModalActive(!updateModalActive)
    }, [updateModalActive]);

    const handleUpdate = () => {
        if (!StringUtil.isEmpty(editOrderAmount)) {
            let unixDate = new Date()
            unixDate = (unixDate.getTime() / 1000).toFixed(0)

            let payload = {
                id: originalID,
                date: unixDate,
                amount: editOrderAmount,
                blood_type: editOrderBloodType
            }
            console.log("payload in handle update: ", payload)

            api.updateBloodOrderInfo(payload, (isSuccess, response, error) => {
                if (isSuccess) {
                    setUpdateModalActive(false)
                    getOrders()
                } else {
                    console.log("ERROR IN UPDATE BLOOD ORDER, ", error)
                }
            })

            if (checked) {
                api.sendOrder(payload, (isSuccess, response, error) => {
                    if (isSuccess) {
                        console.log("ORDER SENT")
                    } else {
                        console.log("FAILED TO SEND ORDER,  ", error)
                    }
                })
            }
        } else {
            checkError(editOrderAmount);
        }
    };
    ////////////////
    
    //bulk actions (delete/send)
    const [deleteModal, setDeleteModal] = useState(false);
    const handleDeleteModalChanged = useCallback(() => setDeleteModal(!deleteModal), [deleteModal])
    const [sendModal, setSendModal] = useState(false);
    const handleSendModalChanged = useCallback(() => setSendModal(!sendModal), [sendModal])

    const promotedBulkActions = [
        {
            content: 'Delete orders',
            onAction: handleDeleteModalChanged
        },
        {
            content: 'Send orders',
            onAction: handleSendModalChanged
        },
    ];
    const handleDelete = () => {
        let error = null;
        let flag = true;
        for (let i = 0; i < selectedItems.length; i++) {
            let payload = {
                id: selectedItems[i]
            }
            api.deleteBloodOrder(payload, (isSuccess, response, error) => {
                if (isSuccess) {
                    flag = true
                } else {
                    error = error
                }
            })
        }
        if (!flag) {
            console.log("ERROR IN DELETING ORDERS, ", error)
        }

        handleDeleteModalChanged()
        getOrders()
    }
    const handleSend = () => {
        let error = null;
        let flag = true;
        for (let i = 0; i < selectedItems.length; i++) {
            let payload = {
                id: selectedItems[i]
            }
            api.sendOrder(payload, (isSuccess, response, error) => {
                if (isSuccess) {
                    flag = true
                } else {
                    flag = false
                    error = error
                }
            })
        }
        if (flag) getOrders()
        else console.log("ERROR IN SENDING ORDERS, ", error)

        handleSendModalChanged()
    }
    ///////////////////

    useEffect(() => {
        getOrders();
    }, [])

    function renderItem(item) {
        const { order_id, order_date, amount, blood_type, status } = item;
        let variation = "positive"
        if (status === "unsent") variation = "negative"
        return (
            <ResourceItem
                id={order_id}
                url={null}
                accessibilityLabel={`View details for ${order_id}`}
                onClick={handleEditItem}
            >
                <h3>
                    <TextStyle variation="strong">Order ID: {order_id}</TextStyle>
                </h3>
                <div>Date: {order_date}</div>
                <div>Amount: {amount}</div>
                <div>Blood Type: {blood_type}</div>
                <div>Status: {" "}
                    <TextStyle variation={variation}>
                        {status}
                    </TextStyle>
                </div>

            </ResourceItem>
        );
    }
    /////////////////////////




    return (
        <PageLayout userName={props.userName}
            userType={props.userType}
            title={"Managing Blood Orders"}
            expectedUserType={"hospital"}
            navigationArray={[
                {
                    label: 'Manage Blood Orders',
                    icon: OrdersMajorMonotone,
                    onClick: () => {
                    },
                },
                {
                    label: 'View Sent Orders Status',
                    icon: ArchiveMajorMonotone,
                    onClick: () => {
                        Router.push("/hospital/blood_order_status").then(() => {
                            console.log("Route successfully from 'hospital/manage_blood_order' to 'hospital/blood_order_status'");
                        })
                    },
                }
            ]}
        >
            <Page
                fullWidth
                title="Manage Blood Orders"
                subtitle={bloodOrders.length ? "Send, create, update or delete orders" : "No orders available"}
                primaryAction={{ content: 'Create order', icon: PlusMinor, destructive: true, onAction: handleChange }}
            >
                {/* create order modal */}
                <Modal
                    open={active}
                    onClose={handleClose}
                    title="Create new blood order"
                    primaryAction={{
                        content: 'Create',
                        onAction: handleCreate,
                    }}
                    secondaryActions={[
                        {
                            content: 'Cancel',
                            onAction: handleClose,
                        },
                    ]}
                >
                    <Modal.Section>
                        <FormLayout>
                            <FormLayout.Group condensed>
                                <TextField value={amount} type="number" label="Amount (mL)" onChange={handleAmountChange} />
                                <Select value={bloodtype} options={options} label="Blood Type" onChange={handleBloodTypeChange} />
                            </FormLayout.Group>
                        </FormLayout>
                    </Modal.Section>
                </Modal>

                {/* edit order modal */}
                <Modal
                    open={updateModalActive}
                    onClose={handleUpdateModalClose}
                    title="Edit this order"
                    primaryAction={{
                        content: 'Confirm',
                        onAction: handleUpdate,
                    }}
                    secondaryActions={[
                        {
                            content: 'Cancel',
                            onAction: handleUpdateModalClose,
                        },
                    ]}
                >
                    <Modal.Section>
                        <Stack vertical>
                            <Stack.Item>
                                <Layout>
                                    <Layout.AnnotatedSection
                                        description="Specify the new amount and blood type for the order"
                                    >
                                        <FormLayout>
                                            <FormLayout.Group condensed>
                                                <TextField 
                                                    value={editOrderAmount} 
                                                    type="number" 
                                                    label="Amount (ml)" 
                                                    onChange={useCallback((value) => {
                                                        seteditOrderAmount(value)
                                                    }, [])}
                                                    error={amountError === '' ? null : amountError}
                                                />
                                                <Select 
                                                    value={editOrderBloodType} 
                                                    options={options} 
                                                    label="Blood Type" 
                                                    onChange={useCallback((value) => {
                                                        seteditOrderBloodType(value)
                                                    }, [])} 
                                                />
                                            </FormLayout.Group>

                                            <div>
                                                <TextStyle variation="strong">
                                                     Original Order Info: <br></br>
                                                </TextStyle>
                                                ID: {originalID}<br></br>
                                                Amount (mL): {originalAmount}<br></br>
                                                Blood Type: {originalBloodType}<br></br>
                                                Status: 
                                                <TextStyle variation="negative">
                                                     {originalStatus}
                                                </TextStyle><br></br>

                                            </div>
                                            <Checkbox
                                                label="Send Order (You will not be able to edit this order later if you send it)"
                                                checked={checked}
                                                onChange={handleCheckboxChange}
                                            />
                                        </FormLayout>
                                    </Layout.AnnotatedSection>
                                </Layout>
                            </Stack.Item>
                        </Stack>
                    </Modal.Section>
                </Modal>

                {/* delete modal */}
                <Modal
                    open={deleteModal}
                    onClose={handleDeleteModalChanged}
                    title="Delete orders"
                    primaryAction={{
                        content: 'Confirm',
                        onAction: handleDelete,
                    }}
                    secondaryActions={[
                        {
                            content: 'Cancel',
                            onAction: handleDeleteModalChanged,
                        },
                    ]}
                >
                    <Modal.Section>
                        <Stack vertical>
                            <Stack.Item>
                                <TextStyle variation="strong">Are you sure you want to delete these orders ?</TextStyle>
                            </Stack.Item>
                        </Stack>
                    </Modal.Section>
                </Modal>

                {/* send modal */}
                <Modal
                    open={sendModal}
                    onClose={handleSendModalChanged}
                    title="Send orders"
                    primaryAction={{
                        content: 'Confirm',
                        onAction: handleSend,
                    }}
                    secondaryActions={[
                        {
                            content: 'Cancel',
                            onAction: handleSendModalChanged,
                        },
                    ]}
                >
                    <Modal.Section>
                        <Stack vertical>
                            <Stack.Item>
                                <TextStyle variation="strong">Are you sure you want to send these orders? You will not be able to edit them later</TextStyle>
                            </Stack.Item>
                        </Stack>
                    </Modal.Section>
                </Modal>

                {/* display order list */}
                <Layout>
                    <Layout.Section>
                        <Card title="Blood Orders">
                            <ResourceList
                                resourceName={resourceName}
                                items={bloodOrders}
                                selectedItems={selectedItems}
                                onSelectionChange={(item) => {
                                    setSelectedItems(item);
                                    console.log(selectedItems);
                                }}
                                sortValue={sortValue}
                                sortOptions={[
                                    { label: 'Date (Descending)', value: 'DATE_DESC' },
                                    { label: 'Date (Ascending)', value: 'DATE_ASC' },
                                ]}
                                onSortChange={(selected) => {
                                    setSortValue(selected);
                                    setBloodOrders(sortBloodOrders(selected, bloodOrders));
                                    setRawBloodOrders(sortBloodOrders(selected, rawBloodOrders));
                                    console.log(`Sort option changed to ${selected}.`);
                                }}
                                renderItem={renderItem}
                                onSelectionChange={(item) => {
                                    setSelectedItems(item);
                                    console.log(selectedItems);
                                }}
                                filterControl={filterControl}
                                loading={isLoading}
                                promotedBulkActions={promotedBulkActions}
                            />
                        </Card>
                    </Layout.Section>
                </Layout>

            </Page>

            {/* pagination */}
            <div className="outer">
                <div className="inner">
                    <Pagination
                        hasPrevious
                        onPrevious={() => {
                            if (offset > 0) {
                                offset -= Constants.LIMIT
                                getOrders()
                            }
                        }}
                        hasNext
                        onNext={() => {
                            if (rawBloodOrders.length !== 0) {
                                // MEANING THAT IT HAS NOT REACHED THE END OF THE LIST
                                if (rawBloodOrders.length === 3) {
                                    offset += Constants.LIMIT
                                    getOrders()
                                }
                            }
                        }}
                    />
                </div>
            </div>
        </PageLayout>
    )
}

export default connect(state => ({
    userName: state.LoginReducer.userName,
    email: state.LoginReducer.email,
    userType: state.LoginReducer.userType,
    userId: state.LoginReducer.userId
}))(BloodOrder);