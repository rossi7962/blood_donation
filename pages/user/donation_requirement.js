import React, {useEffect, useState, useCallback} from "react";
import Router from "next/router";
import {connect} from 'react-redux';
import PageLayout from "../../src/components/PageLayout.js"
import {
    Page,
    TextField,
    Select,
    FormLayout,
    Checkbox,
    Layout,
    Card,
    Button,
    Toast
} from "@shopify/polaris"
import {ProfileMajorMonotone, LockMajorMonotone, ArrowLeftMinor, VocabularyMajorMonotone} from "@shopify/polaris-icons";
import AuthenticationRouteUtils from "../../src/utils/AuthenRouteUtils";
import api from "../../config/api";
import StringUtil from "../../src/utils/StringUtils";
import LoginAction from "../../src/redux/actions/LoginAction";

function DonationRequirement(props) {
    const [userHeight, setUserHeight] = useState("");
    const [userWeight, setUserWeight] = useState("");
    const [userGender, setUserGender] = useState("");
    const [userBloodType, setUserBloodType] = useState("");
    const [userHasTattooLast12Month, setUserHasTattooLast12Month] = useState(null);
    const [userHasCholesterol, setUserHasCholesterol] = useState(null);
    const [userHasPositiveHIV, setUserHasPositiveHIV] = useState(null);
    const [userHasInfectiousDisease, setUserHasInfectiousDisease] = useState(null);
    const [userHasCancer, setUserHasCancer] = useState(null);

    const [loadingSaveChange, setLoadingSaveChange] = useState(false);

    const genderOptions = [
        {label: 'Male', value: 'male'},
        {label: 'Female', value: 'female'},
        {label: 'Other', value: '3'},
    ];

    const bloodTypeOptions = [
        {label: 'A+', value: 'A+'},
        {label: 'A-', value: 'A-'},
        {label: 'B+', value: 'B+'},
        {label: 'B-', value: 'B-'},
        {label: 'O+', value: 'O+'},
        {label: 'O', value: 'O'},
        {label: 'AB+', value: 'AB+'},
        {label: 'AB-', value: 'AB-'},
    ];

    const handleClickSaveChangeAdditionalData = () => {
        setLoadingSaveChange(true);
        let payload = {
            email: props.email,
            name: props.userName,

            height: userHeight,
            weight: userWeight,
            gender: userGender,
            blood_type: userBloodType,

            tattoo_last_12_month: userHasTattooLast12Month === null ? null : userHasTattooLast12Month ? 1 : 0,
            cholesterol: userHasCholesterol === null ? null : userHasCholesterol ? 1 : 0,
            positive_test_HIV: userHasPositiveHIV === null ? null : userHasPositiveHIV ? 1 : 0,
            infectious_disease: userHasInfectiousDisease === null ? null : userHasInfectiousDisease ? 1 : 0,
            cancer: userHasCancer === null ? null : userHasCancer ? 1 : 0
        };

        api.updateUserProfile(payload, (isSuccess, response, error) => {
            setLoadingSaveChange(false);
            console.log(response);
            if (isSuccess) {

                props.dispatch(LoginAction.logIn({
                    name: props.userName,
                    email: props.email,
                    userId: props.userId,
                    userType: props.userType,
                    address: props.address,

                    blood_type: userBloodType,
                    donation_requirement_data: {
                        height: userHeight,
                        weight: userWeight,
                        gender: userGender,
                        tattoo_last_12_month: userHasTattooLast12Month,
                        cholesterol: userHasCholesterol,
                        positive_test_HIV: userHasPositiveHIV,
                        infectious_disease: userHasInfectiousDisease,
                        cancer: userHasCancer,
                    }
                }));

                showToastWithMessage("Update profile successfully");
            } else {
                if (error.response) {
                    switch (error.response.status) {
                        case 500:
                            showToastWithMessage("Error: internal server error");
                            break;
                        default:
                            showToastWithMessage("Unexpected error");
                            break;
                    }
                } else showToastWithMessage("Unexpected error");
            }
        })
    };

    useEffect(() => {
        if (props.userName) {
            if (props.userType !== "donor") {
                Router.push("/user/profile");
                return;
            }

            setUserWeight(props.donation_requirement_data.weight);
            setUserHeight(props.donation_requirement_data.height);
            setUserGender(props.donation_requirement_data.gender ? props.donation_requirement_data.gender : "male");
            setUserBloodType(props.blood_type ? props.blood_type : "A+");

            let a = props.donation_requirement_data.infectious_disease;
            let b = props.donation_requirement_data.positive_test_HIV;
            let c = props.donation_requirement_data.cancer;
            let d = props.donation_requirement_data.tattoo_last_12_month;
            let e = props.donation_requirement_data.cholesterol;

            setUserHasInfectiousDisease(a === null ? null : a !== 0);
            setUserHasPositiveHIV(b === null ? null : b !== 0);
            setUserHasCancer(c === null ? null : c !== 0);
            setUserHasTattooLast12Month(d === null ? null : d !== 0);
            setUserHasCholesterol(e === null ? null : e !== 0);
        }
    }, [props.userName]);

    const [activeToast, setActiveToast] = useState(false);
    const [toastMessage, setToastMessage] = useState("");
    const toggleActiveToast = useCallback(() => setActiveToast((active) => !active), []);
    const ToastSuccessUpdateProfile = activeToast ? (
        <Toast content={toastMessage} onDismiss={toggleActiveToast}/>
    ) : null;

    const showToastWithMessage = (message) => {
        setToastMessage(message);
        setActiveToast(true);
    };

    return (
        <PageLayout userName={props.userName}
                    userType={props.userType}

                    sideBarTitle={"User Settings"}
                    navigationArray={[
                        {
                            label: 'Profile',
                            icon: ProfileMajorMonotone,
                            onClick: () => {
                                Router.push("/user/profile").then(() => {
                                });
                            },
                        },
                        {
                            label: 'Health Index',
                            icon: VocabularyMajorMonotone,
                            disabled: !props.userType || props.userType !== "donor",
                            onClick: () => {
                                //DO NOT THING BECAUSE ALREADY AT THIS ROUTE
                            },
                        },
                        {
                            label: 'Security',
                            icon: LockMajorMonotone,
                            onClick: () => {
                                //DO NOT THING BECAUSE ALREADY AT THIS ROUTE
                                Router.push("/user/security").then(() => {
                                });
                            },
                        },
                        props.userName ?
                            {
                                label: 'Back to ' + (props.userType.charAt(0).toUpperCase() + StringUtil.roleToString(props.userType).slice(1)) + ' page',
                                icon: ArrowLeftMinor,
                                onClick: () => {
                                    let route = AuthenticationRouteUtils.stringRouteByRole(props.userType);
                                    Router.push(route).then(() => {
                                    });
                                }
                            } : {
                                label: 'Back',
                                icon: ArrowLeftMinor,
                                disabled: true
                            },
                    ]}

                    pageTitle={"Requirement - Blood Donation"}
        >
            <Page>
                {props.userType === "donor" ?
                    <Layout.AnnotatedSection
                        title="Blood Donation Required Information"
                        description="You must have this section filled so that you can start donating blood"
                    >
                        <Card sectioned>
                            <FormLayout>
                                <TextField
                                    label="Height (cm)"
                                    type="number"
                                    value={userHeight ? userHeight.toString() : null}
                                    onChange={setUserHeight}
                                />
                                <TextField
                                    label="Weight (kg)"
                                    type="number"
                                    value={userWeight ? userWeight.toString() : null}
                                    onChange={setUserWeight}
                                />
                                <Select
                                    label="Gender"
                                    options={genderOptions}
                                    onChange={(newValue) => {
                                        setUserGender(newValue);
                                    }}
                                    value={userGender}
                                />
                                <Select
                                    label={"Blood Type"}
                                    options={bloodTypeOptions}
                                    onChange={(newValue) => {
                                        setUserBloodType(newValue);
                                    }}
                                    value={userBloodType}
                                />

                                Have you had tattoo for the last 3 months?
                                <Checkbox
                                    label="Yes"
                                    checked={userHasTattooLast12Month}
                                    onChange={() => setUserHasTattooLast12Month(true)}
                                />
                                <Checkbox
                                    label="No"
                                    checked={userHasTattooLast12Month === null ? false : !userHasTattooLast12Month}
                                    onChange={() => setUserHasTattooLast12Month(false)}
                                />

                                Does your blood have high cholesterol rate?
                                <Checkbox
                                    label="Yes"
                                    checked={userHasCholesterol}
                                    onChange={() => setUserHasCholesterol(true)}
                                />
                                <Checkbox
                                    label="No"
                                    checked={userHasCholesterol === null ? false : !userHasCholesterol}
                                    onChange={() => setUserHasCholesterol(false)}
                                />

                                Do you have positive test result with HIV?
                                <Checkbox
                                    label="Yes"
                                    checked={userHasPositiveHIV}
                                    onChange={() => setUserHasPositiveHIV(true)}
                                />
                                <Checkbox
                                    label="No"
                                    checked={userHasPositiveHIV === null ? false : !userHasPositiveHIV}
                                    onChange={() => setUserHasPositiveHIV(false)}
                                />

                                Have you ever infected with infectious disease?
                                <Checkbox
                                    label="Yes"
                                    checked={userHasInfectiousDisease}
                                    onChange={() => setUserHasInfectiousDisease(true)}
                                />
                                <Checkbox
                                    label="No"
                                    checked={userHasInfectiousDisease === null ? false : !userHasInfectiousDisease}
                                    onChange={() => setUserHasInfectiousDisease(false)}
                                />

                                Do you have cancer?
                                <Checkbox
                                    label="Yes"
                                    checked={userHasCancer}
                                    onChange={() => setUserHasCancer(true)}
                                />
                                <Checkbox
                                    label="No"
                                    checked={userHasCancer === null ? false : !userHasCancer}
                                    onChange={() => setUserHasCancer(false)}
                                />
                                <Button primary loading={loadingSaveChange}
                                        onClick={handleClickSaveChangeAdditionalData}>Save
                                    Changes</Button>
                            </FormLayout>
                        </Card>
                    </Layout.AnnotatedSection> : null}
            </Page>
            {ToastSuccessUpdateProfile}
        </PageLayout>
    );
}

export default connect(state => ({
    userName: state.LoginReducer.userName,
    email: state.LoginReducer.email,
    userType: state.LoginReducer.userType,
    userId: state.LoginReducer.userId,
    address: state.LoginReducer.address,
    blood_type: state.LoginReducer.blood_type,
    dob: state.LoginReducer.dob,
    donation_requirement_data: state.LoginReducer.donation_requirement_data
}))(DonationRequirement);