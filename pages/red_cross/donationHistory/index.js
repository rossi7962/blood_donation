import React, { useEffect, useState, useCallback } from "react";
import { useRouter } from "next/router";
import { connect } from "react-redux";
import PageLayout from "../../../src/components/PageLayout.js";
import DonationList from "../../../src/components/DonationList.js";
import {
  Card,
  Page,
  Scrollable,
  Layout,
  Filters,
  ChoiceList,
} from "@shopify/polaris";
import {
  OrdersMajorMonotone,
  CustomersMajorMonotone,
  HeartMajorMonotone,
  ActivitiesMajorMonotone,
  StoreMajorMonotone,
} from "@shopify/polaris-icons";
import api from "../../../config/api";
import useDebounce from "../../../src/utils/useDebounce";
function donationHistory(props) {
  const router = useRouter();
  const [Donations, setDonations] = useState([]);
  const [taggedStatus, setTaggedStatus] = useState(null);
  const [queryValue, setQueryValue] = useState(null);
  const [sortValue, setSortValue] = useState("donate_date__ASC");
  const [isLoading, setIsLoading] = useState(true);
  const [page, setPage] = useState(0);
  const [hasPrevious, setHasPrevious] = useState(false);
  const [hasNext, setHasNext] = useState(true);

  const debouncedSearchTerm = useDebounce(queryValue, 500);
  const handleOnPrevious = () => {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0;
    router.push(
      "/red_cross/donationHistory?page=" + (parseInt(page) - 1),
      undefined,
      {
        shallow: true,
      }
    );
  };
  const handleOnNext = () => {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0;
    router.push(
      "/red_cross/donationHistory?page=" + (parseInt(page) + 1),
      undefined,
      {
        shallow: true,
      }
    );
  };

  const handleSortValueChange = useCallback((value) => setSortValue(value), []);

  const handleTaggedStatusChange = useCallback(
    (value) => setTaggedStatus(value),
    []
  );
  const handleQueryValueChange = useCallback(
    (value) => setQueryValue(value),
    []
  );
  const handleTaggedStatusRemove = useCallback(() => setTaggedStatus(null), []);
  const handleQueryValueRemove = useCallback(() => setQueryValue(null), []);
  const handleClearAll = useCallback(() => {
    handleTaggedStatusRemove();
    handleQueryValueRemove();
  }, [handleQueryValueRemove, handleTaggedStatusRemove]);
  const filters = [
    {
      key: "Donation Status",
      label: "Donation status",
      filter: (
        <ChoiceList
          title="Donation status"
          titleHidden
          choices={[
            { label: "active", value: "active" },
            { label: "stored", value: "stored" },
            { label: "rejected", value: "rejected" },
          ]}
          selected={taggedStatus || []}
          onChange={handleTaggedStatusChange}
          allowMultiple
        />
      ),
      shortcut: true,
    },
  ];
  const appliedFilters = [];
  if (!isEmpty(taggedStatus)) {
    const key = "taggedStatus";
    appliedFilters.push({
      key,
      label: disambiguateLabel(key, taggedStatus),
      onRemove: handleTaggedStatusRemove,
    });
  }
  const filterControl = (
    <Filters
      queryValue={queryValue}
      filters={filters}
      appliedFilters={appliedFilters}
      onQueryChange={handleQueryValueChange}
      onQueryClear={handleQueryValueRemove}
      onClearAll={handleClearAll}
    />
  );
  const sortOptions = [
    { label: "Newest Donate Date", value: "donate_date__DESC" },
    { label: "Oldest Donate Date", value: "donate_date__ASC" },
  ];
  const getAcceptedBloodDonation = useCallback(
    (searchFilters, statusFilters, sortFilter, page) => {
      const params1 = searchFilters ? "?name=" + searchFilters : "";
      const params2 = statusFilters
        ? searchFilters
          ? "&status=" + statusFilters
          : "?status=" + statusFilters
        : "";
      const params3 = sortFilter
        ? statusFilters
          ? "&order_by=" + sortFilter
          : searchFilters
          ? "&order_by=" + sortFilter
          : "?order_by=" + sortFilter
        : "";
      const params = params1 + params2 + params3 + "&limit=5&page=" + page;
      api.getAcceptedBloodDonation(
        null,
        params,
        (isSuccess, response, error) => {
          if (isSuccess) {
            setIsLoading(false);
            console.log("data", response.data.data);
            setDonations(response.data.data);
          } else console.log(error.response);
        }
      );
    },
    [Donations]
  );
  useEffect(() => {
    console.log("tagged", taggedStatus, sortValue);
    if (taggedStatus || sortValue) {
      setIsLoading(true);
      // Fire off our API call
      if (parseInt(page) == 0) {
        getAcceptedBloodDonation(
          debouncedSearchTerm,
          taggedStatus,
          sortValue,
          page
        );
      } else
        router.push("/red_cross/donationHistory?page=0", undefined, {
          shallow: true,
        });
    }
  }, [debouncedSearchTerm, taggedStatus, sortValue]);
  useEffect(() => {
    console.log(router.query.page);
    if (router.query.page) {
      if (parseInt(router.query.page) == 0) {
        setHasPrevious(false);
      } else if (parseInt(router.query.page) > 0) setHasPrevious(true);
      if (parseInt(router.query.page) == 12) setHasNext(false);
      else if (parseInt(router.query.page) < 12) setHasNext(true);
      setPage(router.query.page);
      setIsLoading(true);
      // Fire off our API call
      getAcceptedBloodDonation(
        debouncedSearchTerm,
        taggedStatus,
        sortValue,
        router.query.page
      );
    }
  }, [router.query.page, debouncedSearchTerm]);

  useEffect(() => {
    //COMPONENT DID MOUNT
    // Make sure we have a value (user has entered something in input)
    if (debouncedSearchTerm) {
      setHasPrevious(false);
      // Set isSearching state
      console.log("search", debouncedSearchTerm);
      router.push("/red_cross/donationHistory?page=0", undefined, {
        shallow: true,
      });
    } else getAcceptedBloodDonation(null, taggedStatus, sortValue, page);
  }, [debouncedSearchTerm]);
  return (
    <PageLayout
      userName={props.userName}
      userType={props.userType}
      expectedUserType={"red_cross"}
      navigationArray={[
        {
          label: "Donors Management",
          icon: CustomersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/getDonors").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingDonation' to 'red_cross/getDonors'"
              );
            });
          },
        },
        {
          label: "Organizers Management",
          icon: CustomersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/getOrganizers").then(() => {
              console.log(
                "Route successfully from 'red_cross/getDonors' to 'red_cross/getOrganizers'"
              );
            });
          },
        },
        {
          label: "Hospitals Management",
          icon: CustomersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/getHospitals").then(() => {
              console.log(
                "Route successfully from 'red_cross/getDonors' to 'red_cross/getHospitals'"
              );
            });
          },
        },
        {
          label: "Pending Events Requests",
          icon: ActivitiesMajorMonotone,
          onClick: () => {
            router.push("/red_cross/pendingEvents").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingDonation' to 'red_cross/pendingEvents'"
              );
            });
          },
        },
        {
          label: " Pending Orders Requests ",
          icon: OrdersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/pendingOrders").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingDonation' to 'red_cross/pendingOrders'"
              );
            });
          },
        },
        {
          label: " Pending Donation ",
          icon: HeartMajorMonotone,
          onClick: () => {
            router.push("/red_cross/pendingDonation").then(() => {
              console.log(
                "Route successfully from 'red_cross/donationHistory' to 'red_cross/eventsHistorys'"
              );
            });
          },
        },
        {
          label: "Events Management",
          icon: ActivitiesMajorMonotone,
          onClick: () => {
            router.push("/red_cross/eventsHistory").then(() => {
              console.log(
                "Route successfully from 'red_cross/donationHistory' to 'red_cross/eventsHistorys'"
              );
            });
          },
        },
        {
          label: " Orders Management ",
          icon: OrdersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/orderHistory").then(() => {
              console.log(
                "Route successfully from 'red_cross/donationHIstory' to 'red_cross/orderHistorys'"
              );
            });
          },
        },
        {
          label: " Donation Management",
          icon: HeartMajorMonotone,
          onClick: () => {},
        },
        {
          label: " Blood Store ",
          icon: StoreMajorMonotone,
          onClick: () => {
            router.push("/red_cross/bloodstore").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingDonations' to 'red_cross/bloodstore'"
              );
            });
          },
        },
      ]}
    >
      <Page
        title="Donation Management"
        subtitle={
          Donations.length ? "Donation History" : "No Donations available"
        }
      >
        <Layout>
          <Layout.Section>
            <Card>
              {/* <Scrollable shadow style={{ height: "500px" }}> */}
              <DonationList
                data={Donations}
                router={router}
                loading={isLoading}
                filterControl={filterControl}
                sortValue={sortValue}
                sortOptions={sortOptions}
                onSortChange={handleSortValueChange}
                hasNext={hasNext}
                hasPrevious={hasPrevious}
                onPrevious={handleOnPrevious}
                onNext={handleOnNext}
              />
              {/* </Scrollable> */}
            </Card>
          </Layout.Section>
        </Layout>
      </Page>
    </PageLayout>
  );
}
function disambiguateLabel(key, value) {
  switch (key) {
    case "taggedWith":
      return `Tagged with ${value}`;
    case "taggedStatus":
      return value.map((val) => `Donation ${val}`).join(", ");
    default:
      return value;
  }
}
function isEmpty(value) {
  if (Array.isArray(value)) {
    return value.length === 0;
  } else {
    return value === "" || value == null;
  }
}
export default connect((state) => ({
  userName: state.LoginReducer.userName,
  email: state.LoginReducer.email,
  userType: state.LoginReducer.userType,
  userId: state.LoginReducer.userId,
}))(donationHistory);
