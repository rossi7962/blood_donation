import React, { useEffect, useState, useCallback } from "react";
import { useRouter } from "next/router";
import { connect } from "react-redux";
import PageLayout from "../../../src/components/PageLayout.js";
import { Card, Page, FormLayout, TextField } from "@shopify/polaris";
import { ArrowLeftMinor } from "@shopify/polaris-icons";
import api from "../../../config/api";
function getOrganizersDetail(props) {
  const router = useRouter();
  const [organizer, setOrganizer] = useState(null);
  const { organizerId } = router.query;
  // const promotedBulkActions = [];

  // const bulkActions = [];
  const getOrganizersDetail = useCallback(
    (organizerId) => {
      api.getOrganizersDetail(
        null,
        organizerId,
        (isSuccess, response, error) => {
          console.log(response.data.data);
          if (isSuccess) setOrganizer(response.data.data[0]);
          else console.log(error.response);
        }
      );
    },
    [organizer]
  );
  useEffect(() => {
    //COMPONENT DID MOUNT
    if (organizerId) getOrganizersDetail(organizerId);
  }, [organizerId]);
  return (
    <PageLayout
      userName={props.userName}
      userType={props.userType}
      expectedUserType={"red_cross"}
      navigationArray={[
        {
          label: "Go Back",
          icon: ArrowLeftMinor,
          onClick: () => {
            router.back();
          },
        },
      ]}
    >
      <Page title="Organizer Information">
        {organizer && (
          <Card>
            <FormLayout>
              <TextField
                label="Email"
                placeholder="Email"
                value={organizer.email}
                readOnly
              />
              <TextField
                label="Username"
                placeholder="Username"
                value={organizer.name}
                readOnly
              />
            </FormLayout>
          </Card>
        )}
      </Page>
    </PageLayout>
  );
}
export default connect((state) => ({
  userName: state.LoginReducer.userName,
  email: state.LoginReducer.email,
  userType: state.LoginReducer.userType,
  userId: state.LoginReducer.userId,
}))(getOrganizersDetail);
