import React, { useEffect, useState, useCallback } from "react";
import { useRouter } from "next/router";
import { connect } from "react-redux";
import PageLayout from "../../../src/components/PageLayout.js";
import { Card, Page, FormLayout, TextField } from "@shopify/polaris";
import { ArrowLeftMinor } from "@shopify/polaris-icons";
import api from "../../../config/api";
function getHospitalsDetail(props) {
  const router = useRouter();
  const [hospital, setHospital] = useState(null);
  const { hospitalId } = router.query;
  // const promotedBulkActions = [];

  // const bulkActions = [];
  const getHospitalsDetail = useCallback(
    (hospitalId) => {
      api.getHospitalsDetail(null, hospitalId, (isSuccess, response, error) => {
        console.log(response.data.data);
        if (isSuccess) setHospital(response.data.data[0]);
        else console.log(error.response);
      });
    },
    [hospital]
  );
  useEffect(() => {
    //COMPONENT DID MOUNT
    if (hospitalId) getHospitalsDetail(hospitalId);
  }, [hospitalId]);
  return (
    <PageLayout
      userName={props.userName}
      userType={props.userType}
      expectedUserType={"red_cross"}
      navigationArray={[
        {
          label: "Go Back",
          icon: ArrowLeftMinor,
          onClick: () => {
            router.back();
          },
        },
      ]}
    >
      <Page title="Hospital Information">
        {hospital && (
          <Card>
            <FormLayout>
              <TextField
                label="Email"
                placeholder="Email"
                value={hospital.email}
                readOnly
              />
              <TextField
                label="Username"
                placeholder="Username"
                value={hospital.name}
                readOnly
              />
            </FormLayout>
          </Card>
        )}
      </Page>
    </PageLayout>
  );
}
export default connect((state) => ({
  userName: state.LoginReducer.userName,
  email: state.LoginReducer.email,
  userType: state.LoginReducer.userType,
  userId: state.LoginReducer.userId,
}))(getHospitalsDetail);
