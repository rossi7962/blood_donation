import React, { useEffect, useState, useCallback } from "react";
import { useRouter } from "next/router";
import { connect } from "react-redux";
import PageLayout from "../../../src/components/PageLayout.js";
import {
  Avatar,
  Card,
  Page,
  ResourceList,
  ResourceItem,
  TextStyle,
  Filters,
  Pagination,
} from "@shopify/polaris";
import {
  OrdersMajorMonotone,
  HeartMajorMonotone,
  CustomersMajorMonotone,
  ActivitiesMajorMonotone,
  StoreMajorMonotone,
} from "@shopify/polaris-icons";
import api from "../../../config/api";
import useDebounce from "../../../src/utils/useDebounce";
function getHospitals(props) {
  const router = useRouter();
  const [hospitals, setHospitals] = useState([]);
  const [queryValue, setQueryValue] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [page, setPage] = useState(0);
  const [hasPrevious, setHasPrevious] = useState(false);
  const [hasNext, setHasNext] = useState(true);

  const debouncedSearchTerm = useDebounce(queryValue, 500);
  const handleOnPrevious = () => {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0;
    router.push(
      "/red_cross/getHospitals?page=" + (parseInt(page) - 1),
      undefined,
      {
        shallow: true,
      }
    );
  };
  const handleOnNext = () => {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0;
    router.push(
      "/red_cross/getHospitals?page=" + (parseInt(page) + 1),
      undefined,
      {
        shallow: true,
      }
    );
  };

  const handleQueryValueChange = useCallback(
    (value) => setQueryValue(value),
    []
  );
  const handleQueryValueRemove = useCallback(() => setQueryValue(null), []);
  const handleClearAll = useCallback(() => {
    handleQueryValueRemove();
  }, [handleQueryValueRemove]);
  const filterControl = (
    <Filters
      queryValue={queryValue}
      filters={[]}
      appliedFilters={[]}
      onQueryChange={handleQueryValueChange}
      onQueryClear={handleQueryValueRemove}
      onClearAll={handleClearAll}
    />
  );

  const getHospitals = useCallback(
    (searchFilters, page) => {
      const params1 = searchFilters ? "?name=" + searchFilters : "";
      const params = params1
        ? params1 + "&limit=5&page=" + page
        : params1 + "?limit=5&page=" + page;
      api.getHospitals(null, params, (isSuccess, response, error) => {
        if (isSuccess) {
          setIsLoading(false);
          setHospitals(response.data.data);
        } else console.log(error.response);
      });
    },
    [hospitals]
  );
  useEffect(() => {
    if (router.query.page) {
      if (parseInt(router.query.page) == 0) {
        setHasPrevious(false);
      } else if (parseInt(router.query.page) > 0) setHasPrevious(true);
      if (parseInt(router.query.page) == 12) setHasNext(false);
      else if (parseInt(router.query.page) < 12) setHasNext(true);
      setPage(router.query.page);
      setIsLoading(true);
      // Fire off our API call
      getHospitals(debouncedSearchTerm, router.query.page);
    }
  }, [router.query.page, debouncedSearchTerm]);

  useEffect(() => {
    //COMPONENT DID MOUNT
    // Make sure we have a value (user has entered something in input)
    if (debouncedSearchTerm) {
      setHasPrevious(false);
      // Set isSearching state
      console.log("search", debouncedSearchTerm);
      router.push("/red_cross/getHospitals?page=0", undefined, {
        shallow: true,
      });
    } else getHospitals(null, page);
  }, [debouncedSearchTerm]);
  // useEffect(() => {
  //   //COMPONENT DID UPDATE
  //   getHospitals();
  // }, [hospitals]);
  return (
    <PageLayout
      userName={props.userName}
      userType={props.userType}
      expectedUserType={"red_cross"}
      navigationArray={[
        {
          label: "Donors Management",
          icon: CustomersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/getDonors").then(() => {
              console.log(
                "Route successfully from 'red_cross/getDonors' to 'red_cross/getHospitals'"
              );
            });
          },
        },
        {
          label: "Organizers Management",
          icon: CustomersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/getOrganizers").then(() => {
              console.log(
                "Route successfully from 'red_cross/getDonors' to 'red_cross/getOrganizers'"
              );
            });
          },
        },
        {
          label: "Hospitals Management",
          icon: CustomersMajorMonotone,
          onClick: () => {},
        },
        {
          label: "Pending Events Requests",
          icon: ActivitiesMajorMonotone,
          onClick: () => {
            router.push("/red_cross/pendingEvents").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingDonation' to 'red_cross/pendingEvents'"
              );
            });
          },
        },
        {
          label: " Pending Orders Requests ",
          icon: OrdersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/pendingOrders").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingDonation' to 'red_cross/pendingOrders'"
              );
            });
          },
        },

        {
          label: " Pending Donation ",
          icon: HeartMajorMonotone,
          onClick: () => {
            router.push("/red_cross/pendingDonation").then(() => {
              console.log(
                "Route successfully from 'red_cross/bloodstore' to 'red_cross/pendingDonation'"
              );
            });
          },
        },
        {
          label: "Events Management",
          icon: ActivitiesMajorMonotone,
          onClick: () => {
            router.push("/red_cross/eventsHistory").then(() => {
              console.log(
                "Route successfully from 'red_cross/orderHistory' to 'red_cross/eventsHistorys'"
              );
            });
          },
        },
        {
          label: " Orders Management ",
          icon: OrdersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/orderHistory").then(() => {
              console.log(
                "Route successfully from 'red_cross/eventsHistory' to 'red_cross/ordersHistory'"
              );
            });
          },
        },
        {
          label: " Donation Management",
          icon: HeartMajorMonotone,
          onClick: () => {
            router.push("/red_cross/donationHistory").then(() => {
              console.log(
                "Route successfully from 'red_cross/eventsHIstory' to 'red_cross/donationHistorys'"
              );
            });
          },
        },
        {
          label: " Blood Store ",
          icon: StoreMajorMonotone,
          onClick: () => {
            router.push("/red_cross/bloodstore").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingDonations' to 'red_cross/bloodstore'"
              );
            });
          },
        },
      ]}
    >
      <Page
        title="Hospitals Management"
        subtitle={hospitals.length ? "Hospitals" : "No Hospitals available"}
      >
        <Card>
          <ResourceList
            resourceName={{ singular: "hospital", plural: "hospitals" }}
            items={hospitals}
            renderItem={renderItem}
            filterControl={filterControl}
            loading={isLoading}
          />
          <div className="outer">
            <div className="inner">
              <Pagination
                hasNext={hasNext}
                hasPrevious={hasPrevious}
                onPrevious={handleOnPrevious}
                onNext={handleOnNext}
              />
            </div>
          </div>
        </Card>
      </Page>
    </PageLayout>
  );
  function renderItem(item) {
    const { hospital_id, name, email } = item;
    const shortcutActions = [
      {
        content: "View Detail",
        primary: true,
        onClick: () =>
          router.push(
            "/red_cross/getHospitals/[hospitalId]",
            "/red_cross/getHospitals/" + hospital_id
          ),
      },
    ];
    const media = <Avatar customer size="medium" name={name} />;
    return (
      <ResourceItem
        id={hospital_id}
        media={media}
        shortcutActions={shortcutActions}
      >
        <h3>
          <TextStyle variation="strong">{name}</TextStyle>
        </h3>
        <div>{email}</div>
      </ResourceItem>
    );
  }
}
export default connect((state) => ({
  userName: state.LoginReducer.userName,
  email: state.LoginReducer.email,
  userType: state.LoginReducer.userType,
  userId: state.LoginReducer.userId,
}))(getHospitals);
