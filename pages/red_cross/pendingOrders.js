import React, { useEffect, useState, useCallback } from "react";
import { useRouter } from "next/router";
import { connect } from "react-redux";
import timeConverter from "../../src/utils/timeConverter";
import PageLayout from "../../src/components/PageLayout.js";
import {
  Card,
  Page,
  Avatar,
  Frame,
  Toast,
  ResourceList,
  ResourceItem,
  TextStyle,
  Layout,
  DescriptionList,
  Filters,
  Pagination,
} from "@shopify/polaris";
import {
  CustomersMajorMonotone,
  OrdersMajorMonotone,
  HeartMajorMonotone,
  ActivitiesMajorMonotone,
  StoreMajorMonotone,
} from "@shopify/polaris-icons";
import api from "../../config/api";
import useDebounce from "../../src/utils/useDebounce";
function pendingOrders(props) {
  const router = useRouter();
  const [hospitalOrders, setHospitalOrders] = useState([]);
  const [toastActive, setToastActive] = useState(false);
  const [error, setError] = useState("");
  const [queryValue, setQueryValue] = useState(null);
  const [sortValue, setSortValue] = useState("order_date__ASC");
  const [isLoading, setIsLoading] = useState(true);
  const [page, setPage] = useState(0);
  const [hasPrevious, setHasPrevious] = useState(false);
  const [hasNext, setHasNext] = useState(true);

  const debouncedSearchTerm = useDebounce(queryValue, 500);
  const handleOnPrevious = () => {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0;
    router.push(
      "/red_cross/pendingOrders?page=" + (parseInt(page) - 1),
      undefined,
      {
        shallow: true,
      }
    );
  };
  const handleOnNext = () => {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0;
    router.push(
      "/red_cross/pendingOrders?page=" + (parseInt(page) + 1),
      undefined,
      {
        shallow: true,
      }
    );
  };

  const handleSortValueChange = useCallback((value) => setSortValue(value), []);

  const handleQueryValueChange = useCallback(
    (value) => setQueryValue(value),
    []
  );
  const handleQueryValueRemove = useCallback(() => setQueryValue(null), []);
  const handleClearAll = useCallback(() => {
    handleQueryValueRemove();
  }, [handleQueryValueRemove]);
  const filterControl = (
    <Filters
      queryValue={queryValue}
      filters={[]}
      appliedFilters={[]}
      onQueryChange={handleQueryValueChange}
      onQueryClear={handleQueryValueRemove}
      onClearAll={handleClearAll}
    />
  );
  const sortOptions = [
    { label: "Newes Order Date", value: "order_date__DESC" },
    { label: "Oldest Order Date", value: "order_date__ASC" },
  ];

  const getPendingOrders = useCallback(
    (searchFilters, sortFilter, page) => {
      const params1 = searchFilters ? "?name=" + searchFilters : "";
      const params3 = sortFilter
        ? searchFilters
          ? "&order_by=" + sortFilter
          : "?order_by=" + sortFilter
        : "";
      const params = params1 + params3 + "&limit=5&page=" + page;
      api.getPendingOrders(null, params, (isSuccess, response, error) => {
        if (isSuccess) {
          console.log("data", response.data.data);
          setIsLoading(false);
          setHospitalOrders(response.data.data);
        } else console.log(error.response);
      });
    },
    [hospitalOrders]
  );
  useEffect(() => {
    console.log("tagged", sortValue);
    if (sortValue) {
      setIsLoading(true);
      // Fire off our API call
      getPendingOrders(debouncedSearchTerm, sortValue, page);
    }
  }, [debouncedSearchTerm, sortValue]);
  useEffect(() => {
    console.log(router.query.page);
    if (router.query.page) {
      if (parseInt(router.query.page) == 0) {
        setHasPrevious(false);
      } else if (parseInt(router.query.page) > 0) setHasPrevious(true);
      if (parseInt(router.query.page) == 12) setHasNext(false);
      else if (parseInt(router.query.page) < 12) setHasNext(true);
      setPage(router.query.page);
      setIsLoading(true);
      // Fire off our API call
      getPendingOrders(debouncedSearchTerm, sortValue, router.query.page);
    }
  }, [router.query.page, debouncedSearchTerm]);

  useEffect(() => {
    //COMPONENT DID MOUNT
    // Make sure we have a value (user has entered something in input)
    if (debouncedSearchTerm) {
      setHasPrevious(false);
      // Set isSearching state
      console.log("search", debouncedSearchTerm);
      router.push("/red_cross/pendingOrders?page=0", undefined, {
        shallow: true,
      });
    } else getPendingOrders(null, sortValue, page);
  }, [debouncedSearchTerm]);
  const showToast =
    toastActive === 1 ? (
      <Frame>
        <Toast
          content="Order Accepted"
          onDismiss={() => setToastActive(0)}
        ></Toast>
      </Frame>
    ) : toastActive === 2 ? (
      <Frame>
        <Toast
          content="Order Rejected"
          onDismiss={() => setToastActive(0)}
        ></Toast>
      </Frame>
    ) : toastActive === 3 ? (
      <Frame>
        <Toast content={error} onDismiss={() => setToastActive(0)}></Toast>
      </Frame>
    ) : null;
  const onClickAcceptOrder = useCallback(
    (orderId, amount) => {
      const payload = { amount: amount };
      api.postAcceptOrder(payload, orderId, (isSuccess, response, error) => {
        //IF SUCCESS Delete the accepted order from the pending list
        if (isSuccess) {
          console.log(response.data.data);
          setHospitalOrders([
            ...hospitalOrders.filter((item) => item.order_id !== orderId),
          ]);
          // setModalActive(true);
          setToastActive(1);
        } else {
          console.log(error.response);
          setError(error.response.data.err);
          setToastActive(3);
        }
      });
    },
    [hospitalOrders]
  );
  const onClickRejectOrder = useCallback(
    (orderId) => {
      api.postRejectOrder(null, orderId, (isSuccess, response, error) => {
        //IF SUCCESS Delete the rejected order from the pending list
        if (isSuccess) {
          setHospitalOrders([
            ...hospitalOrders.filter((item) => item.order_id !== orderId),
          ]);
          setToastActive(2);
        } else console.log(error.response);
      });
    },
    [hospitalOrders]
  );
  return (
    <PageLayout
      userName={props.userName}
      userType={props.userType}
      expectedUserType={"red_cross"}
      navigationArray={[
        {
          label: "Donors Management",
          icon: CustomersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/getDonors").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingDonation' to 'red_cross/getDonors'"
              );
            });
          },
        },
        {
          label: "Organizers Management",
          icon: CustomersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/getOrganizers").then(() => {
              console.log(
                "Route successfully from 'red_cross/getDonors' to 'red_cross/getOrganizers'"
              );
            });
          },
        },
        {
          label: "Hospitals Management",
          icon: CustomersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/getHospitals").then(() => {
              console.log(
                "Route successfully from 'red_cross/getDonors' to 'red_cross/getHospitals'"
              );
            });
          },
        },
        {
          label: "Pending Events Requests",
          icon: ActivitiesMajorMonotone,
          onClick: () => {
            router.push("/red_cross/pendingEvents").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingorders' to 'red_cross/pendingEvents'"
              );
            });
          },
        },
        {
          label: " Pending Orders Requests ",
          icon: OrdersMajorMonotone,
          onClick: () => {},
        },
        {
          label: " Pending Donation ",
          icon: HeartMajorMonotone,
          onClick: () => {
            router.push("/red_cross/pendingDonation").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingorders' to 'red_cross/pendingDonation'"
              );
            });
          },
        },
        {
          label: "Events Management",
          icon: ActivitiesMajorMonotone,
          onClick: () => {
            router.push("/red_cross/eventsHistory").then(() => {
              console.log(
                "Route successfully from 'red_cross/orderHistory' to 'red_cross/eventsHistorys'"
              );
            });
          },
        },
        {
          label: " Orders Management ",
          icon: OrdersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/orderHistory").then(() => {
              console.log(
                "Route successfully from 'red_cross/eventsHistory' to 'red_cross/ordersHistory'"
              );
            });
          },
        },
        {
          label: " Donation Management",
          icon: HeartMajorMonotone,
          onClick: () => {
            router.push("/red_cross/donationHistory").then(() => {
              console.log(
                "Route successfully from 'red_cross/eventsHIstory' to 'red_cross/donationHistorys'"
              );
            });
          },
        },
        {
          label: " Blood Store ",
          icon: StoreMajorMonotone,
          onClick: () => {
            router.push("/red_cross/bloodstore").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingorders' to 'red_cross/bloodstore'"
              );
            });
          },
        },
      ]}
    >
      <Page
        title="Pending Orders Management"
        subtitle={
          hospitalOrders.length
            ? "order requests process"
            : "No orders available"
        }
      >
        <Layout>
          <Layout.Section>
            <Card>
              <ResourceList
                resourceName={{ singular: "order", plural: "orders" }}
                items={hospitalOrders}
                idForItem={(item) => item.order_id}
                renderItem={renderItem}
                filterControl={filterControl}
                loading={isLoading}
                sortValue={sortValue}
                sortOptions={sortOptions}
                onSortChange={handleSortValueChange}
              />
              <div className="outer">
                <div className="inner">
                  <Pagination
                    hasNext={hasNext}
                    hasPrevious={hasPrevious}
                    onPrevious={handleOnPrevious}
                    onNext={handleOnNext}
                  />
                </div>
              </div>
            </Card>
          </Layout.Section>
        </Layout>
        {showToast}
      </Page>
    </PageLayout>
  );
  function renderItem(item) {
    const { order_id, blood_type, amount, name, order_date } = item;
    const media = <Avatar size="medium" name={name} />;
    const shortcutActions = [
      {
        content: "Accept Order",
        primary: true,
        onClick: () => onClickAcceptOrder(order_id, amount),
      },
      {
        content: "Reject Order",
        onClick: () => onClickRejectOrder(order_id),
      },
    ];
    const dlItems = [
      {
        term: "Blood Type",
        description: blood_type,
      },
      {
        term: "Amount",
        description: amount,
      },
      {
        term: "Order Date",
        description: timeConverter(order_date),
      },
    ];
    return (
      <ResourceItem
        id={order_id}
        media={media}
        shortcutActions={shortcutActions}
      >
        <h3>
          <TextStyle variation="strong">{name}</TextStyle>
        </h3>
        <DescriptionList items={dlItems} />
      </ResourceItem>
    );
  }
}
export default connect((state) => ({
  userName: state.LoginReducer.userName,
  email: state.LoginReducer.email,
  userType: state.LoginReducer.userType,
  userId: state.LoginReducer.userId,
}))(pendingOrders);
