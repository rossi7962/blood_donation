import React, { useEffect, useState, useCallback } from "react";
import { useRouter } from "next/router";
import { connect } from "react-redux";
import PageLayout from "../../../src/components/PageLayout.js";
import {
  Card,
  Page,
  TextStyle,
  DescriptionList,
  Button,
} from "@shopify/polaris";
import { ArrowLeftMinor } from "@shopify/polaris-icons";
import api from "../../../config/api";
import timeConverter from "../../../src/utils/timeConverter.js";
function getEventsDetail(props) {
  const router = useRouter();
  const [event, setEvent] = useState(null);
  const { eventId } = router.query;
  // const promotedBulkActions = [];

  // const bulkActions = [];
  const getEventsDetail = useCallback(
    (eventId) => {
      api.getAcceptedEventsDetail(
        null,
        eventId,
        (isSuccess, response, error) => {
          console.log("test", response.data.data);
          if (isSuccess) setEvent(response.data.data[0]);
          else console.log(error.response);
        }
      );
    },
    [event]
  );
  useEffect(() => {
    //COMPONENT DID MOUNT
    if (eventId) getEventsDetail(eventId);
  }, [eventId]);
  return (
    <PageLayout
      userName={props.userName}
      userType={props.userType}
      expectedUserType={"red_cross"}
      navigationArray={[
        {
          label: "Go Back",
          icon: ArrowLeftMinor,
          onClick: () => {
            router.back();
          },
        },
      ]}
    >
      <Page title="Event Information">
        {event && (
          <Card>
            <Card.Section>
              <Button
                primary
                onClick={() =>
                  router.push(
                    "/red_cross/getOrganizers/[organizerId",
                    "/red_cross/getOrganizers/" + event.organizer_id
                  )
                }
              >
                View Organizer
              </Button>
            </Card.Section>
            <Card.Section>
              <DescriptionList
                items={[
                  { term: "Event name", description: event.name },
                  { term: "Location", description: event.location },
                  {
                    term: "Event Date",
                    description: timeConverter(event.event_date),
                  },
                  {
                    term: "Status",
                    description: (
                      <p>
                        <TextStyle
                          variation={
                            event.status === "approved"
                              ? "positive"
                              : "negative"
                          }
                        >
                          {event.status}
                        </TextStyle>
                      </p>
                    ),
                  },
                ]}
              />
            </Card.Section>
          </Card>
        )}
      </Page>
    </PageLayout>
  );
}
export default connect((state) => ({
  userName: state.LoginReducer.userName,
  email: state.LoginReducer.email,
  userType: state.LoginReducer.userType,
  userId: state.LoginReducer.userId,
}))(getEventsDetail);
