import React, { useEffect, useState, useCallback } from "react";
import { useRouter } from "next/router";
import timeConverter from "../../../src/utils/timeConverter";
import { connect } from "react-redux";
import PageLayout from "../../../src/components/PageLayout.js";
import {
  Card,
  ResourceItem,
  ResourceList,
  Page,
  TextStyle,
  Avatar,
  ChoiceList,
  Layout,
  Filters,
  Pagination,
} from "@shopify/polaris";
import {
  CustomersMajorMonotone,
  OrdersMajorMonotone,
  HeartMajorMonotone,
  ActivitiesMajorMonotone,
  StoreMajorMonotone,
} from "@shopify/polaris-icons";
import api from "../../../config/api";
import useDebounce from "../../../src/utils/useDebounce";
function eventsHistory(props) {
  const router = useRouter();
  const [organizeEvents, setOrganizeEvents] = useState([]);
  const [queryValue, setQueryValue] = useState(null);
  const [taggedStatus, setTaggedStatus] = useState(null);
  const [sortValue, setSortValue] = useState("event_date__ASC");
  const [isLoading, setIsLoading] = useState(true);
  const [page, setPage] = useState(0);
  const [hasPrevious, setHasPrevious] = useState(false);
  const [hasNext, setHasNext] = useState(true);

  const debouncedSearchTerm = useDebounce(queryValue, 500);
  const handleOnPrevious = () => {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0;
    router.push(
      "/red_cross/eventsHistory?page=" + (parseInt(page) - 1),
      undefined,
      {
        shallow: true,
      }
    );
  };
  const handleOnNext = () => {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0;
    router.push(
      "/red_cross/eventsHistory?page=" + (parseInt(page) + 1),
      undefined,
      {
        shallow: true,
      }
    );
  };
  const handleTaggedStatusChange = useCallback(
    (value) => setTaggedStatus(value),
    []
  );
  const handleSortValueChange = useCallback((value) => setSortValue(value), []);
  const handleTaggedStatusRemove = useCallback(() => setTaggedStatus(null), []);

  const handleQueryValueChange = useCallback(
    (value) => setQueryValue(value),
    []
  );
  const handleQueryValueRemove = useCallback(() => setQueryValue(null), []);
  const handleClearAll = useCallback(() => {
    handleTaggedStatusRemove();
    handleQueryValueRemove();
  }, [handleQueryValueRemove, handleTaggedStatusRemove]);
  const filters = [
    {
      key: "Event Status",
      label: "Event status",
      filter: (
        <ChoiceList
          title="Donation status"
          titleHidden
          choices={[
            { label: "approved", value: "approved" },
            { label: "rejected", value: "rejected" },
          ]}
          selected={taggedStatus || []}
          onChange={handleTaggedStatusChange}
          allowMultiple
        />
      ),
      shortcut: true,
    },
  ];
  const appliedFilters = [];
  if (!isEmpty(taggedStatus)) {
    const key = "taggedStatus";
    appliedFilters.push({
      key,
      label: disambiguateLabel(key, taggedStatus),
      onRemove: handleTaggedStatusRemove,
    });
  }

  const filterControl = (
    <Filters
      filters={filters}
      appliedFilters={appliedFilters}
      queryValue={queryValue}
      onQueryChange={handleQueryValueChange}
      onQueryClear={handleQueryValueRemove}
      onClearAll={handleClearAll}
    />
  );
  const sortOptions = [
    { label: "Newest Event Date", value: "event_date__DESC" },
    { label: "Oldest Event Date", value: "event_date__ASC" },
  ];

  const getAcceptedBloodEvent = useCallback(
    (searchFilters, statusFilters, sortFilter, page) => {
      const params1 = searchFilters ? "?name=" + searchFilters : "";
      const params2 = statusFilters
        ? searchFilters
          ? "&status=" + statusFilters
          : "?status=" + statusFilters
        : "";
      const params3 = sortFilter
        ? statusFilters
          ? "&order_by=" + sortFilter
          : searchFilters
          ? "&order_by=" + sortFilter
          : "?order_by=" + sortFilter
        : "";
      const params = params1 + params2 + params3 + "&limit=5&page=" + page;
      api.getAcceptedBloodEvent(null, params, (isSuccess, response, error) => {
        if (isSuccess) {
          setIsLoading(false);
          setOrganizeEvents(response.data.data);
        } else console.log(error.response);
      });
    },
    [organizeEvents]
  );
  useEffect(() => {
    console.log("tagged", taggedStatus, sortValue);
    if (taggedStatus || sortValue) {
      setIsLoading(true);
      // Fire off our API call
      if (parseInt(page) == 0) {
        getAcceptedBloodEvent(
          debouncedSearchTerm,
          taggedStatus,
          sortValue,
          page
        );
      } else
        router.push("/red_cross/eventsHistory?page=0", undefined, {
          shallow: true,
        });
    }
  }, [debouncedSearchTerm, taggedStatus, sortValue]);

  useEffect(() => {
    if (router.query.page) {
      if (parseInt(router.query.page) == 0) {
        setHasPrevious(false);
      } else if (parseInt(router.query.page) > 0) setHasPrevious(true);
      if (parseInt(router.query.page) == 12) setHasNext(false);
      else if (parseInt(router.query.page) < 100) setHasNext(true);
      setPage(router.query.page);
      setIsLoading(true);
      // Fire off our API call
      getAcceptedBloodEvent(
        debouncedSearchTerm,
        taggedStatus,
        sortValue,
        router.query.page
      );
    }
  }, [router.query.page, debouncedSearchTerm]);

  useEffect(() => {
    //COMPONENT DID MOUNT
    // Make sure we have a value (user has entered something in input)
    if (debouncedSearchTerm) {
      setHasPrevious(false);
      // Set isSearching state
      console.log("search", debouncedSearchTerm);
      router.push("/red_cross/eventsHistory?page=0", undefined, {
        shallow: true,
      });
    } else getAcceptedBloodEvent(null, taggedStatus, sortValue, page);
  }, [debouncedSearchTerm]);
  return (
    <PageLayout
      userName={props.userName}
      userType={props.userType}
      expectedUserType={"red_cross"}
      navigationArray={[
        {
          label: "Donors Management",
          icon: CustomersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/getDonors").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingDonation' to 'red_cross/getDonors'"
              );
            });
          },
        },
        {
          label: "Organizers Management",
          icon: CustomersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/getOrganizers").then(() => {
              console.log(
                "Route successfully from 'red_cross/getDonors' to 'red_cross/getOrganizers'"
              );
            });
          },
        },
        {
          label: "Hospitals Management",
          icon: CustomersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/getHospitals").then(() => {
              console.log(
                "Route successfully from 'red_cross/getDonors' to 'red_cross/getHospitals'"
              );
            });
          },
        },
        {
          label: "Pending Events Requests",
          icon: ActivitiesMajorMonotone,
          onClick: () => {
            //DO NOT THING BECAUSE ALREADY AT THIS ROUTE
            router.push("/red_cross/pendingEvents").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingorders' to 'red_cross/pendingEvents'"
              );
            });
          },
        },
        {
          label: " Pending Orders Requests ",
          icon: OrdersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/pendingOrders").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingEvents' to 'red_cross/pendingOrders'"
              );
            });
          },
        },
        {
          label: " Pending Donation ",
          icon: HeartMajorMonotone,
          onClick: () => {
            router.push("/red_cross/pendingDonation").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingEvents' to 'red_cross/pendingDonation'"
              );
            });
          },
        },
        {
          label: "Events Management",
          icon: ActivitiesMajorMonotone,
          onClick: () => {},
        },
        {
          label: " Orders Management ",
          icon: OrdersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/orderHistory").then(() => {
              console.log(
                "Route successfully from 'red_cross/eventsHistory' to 'red_cross/ordersHistory'"
              );
            });
          },
        },
        {
          label: " Donation Management",
          icon: HeartMajorMonotone,
          onClick: () => {
            router.push("/red_cross/donationHistory").then(() => {
              console.log(
                "Route successfully from 'red_cross/eventsHIstory' to 'red_cross/donationHistorys'"
              );
            });
          },
        },
        {
          label: " Blood Store ",
          icon: StoreMajorMonotone,
          onClick: () => {
            router.push("/red_cross/bloodstore").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingEvents' to 'red_cross/bloodstore'"
              );
            });
          },
        },
      ]}
    >
      <Page
        title="Events Management"
        subtitle={
          organizeEvents.length ? "Event history" : "No events available"
        }
      >
        <Layout>
          <Layout.Section>
            <Card>
              <ResourceList
                resourceName={{
                  singular: "event",
                  plural: "events by event name",
                }}
                items={organizeEvents}
                filterControl={filterControl}
                loading={isLoading}
                sortValue={sortValue}
                sortOptions={sortOptions}
                onSortChange={handleSortValueChange}
                renderItem={(item) => {
                  const {
                    event_id,
                    name,
                    location,
                    event_date,
                    status,
                    organizer_name,
                    organizer_id,
                  } = item;
                  const media = <Avatar size="medium" name={name} />;
                  const shortcutActions = [
                    {
                      content: "View Detail",
                      primary: true,
                      onClick: () =>
                        router.push(
                          "/red_cross/eventsHistory/[organizerId]",
                          "/red_cross/eventsHistory/" + event_id
                        ),
                    },
                    {
                      content: "View Organizer",
                      onClick: () =>
                        router.push(
                          "/red_cross/getOrganizers/[organizerId]",
                          "/red_cross/getOrganizers/" + organizer_id
                        ),
                    },
                  ];
                  return (
                    <ResourceItem
                      id={event_id}
                      media={media}
                      shortcutActions={shortcutActions}
                    >
                      <h3>
                        <TextStyle variation="strong">{name}</TextStyle>
                      </h3>
                      <div>{location}</div>
                      <div>{timeConverter(event_date)}</div>
                      <div>
                        <p>
                          <TextStyle variation="strong">Organizer</TextStyle>:
                          {"\u00A0".repeat(6)}
                          {organizer_name}
                        </p>
                      </div>
                      <div>
                        <TextStyle
                          variation={
                            status === "rejected" ? "negative" : "positive"
                          }
                        >
                          {status}
                        </TextStyle>
                      </div>
                    </ResourceItem>
                  );
                }}
              />
              <div className="outer">
                <div className="inner">
                  <Pagination
                    hasNext={hasNext}
                    hasPrevious={hasPrevious}
                    onPrevious={handleOnPrevious}
                    onNext={handleOnNext}
                  />
                </div>
              </div>
            </Card>
          </Layout.Section>
        </Layout>
      </Page>
    </PageLayout>
  );
}
function disambiguateLabel(key, value) {
  switch (key) {
    case "taggedWith":
      return `Tagged with ${value}`;
    case "taggedStatus":
      return value.map((val) => `Event ${val}`).join(", ");
    default:
      return value;
  }
}
function isEmpty(value) {
  if (Array.isArray(value)) {
    return value.length === 0;
  } else {
    return value === "" || value == null;
  }
}
export default connect((state) => ({
  userName: state.LoginReducer.userName,
  email: state.LoginReducer.email,
  userType: state.LoginReducer.userType,
  userId: state.LoginReducer.userId,
}))(eventsHistory);
