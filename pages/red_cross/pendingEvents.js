import React, { useEffect, useState, useCallback } from "react";
import { useRouter } from "next/router";
import timeConverter from "../../src/utils/timeConverter";
import { connect } from "react-redux";
import PageLayout from "../../src/components/PageLayout.js";
import {
  Card,
  ResourceItem,
  ResourceList,
  Page,
  Toast,
  TextStyle,
  Frame,
  Avatar,
  Filters,
  Pagination,
  Layout,
} from "@shopify/polaris";
import {
  CustomersMajorMonotone,
  OrdersMajorMonotone,
  HeartMajorMonotone,
  ActivitiesMajorMonotone,
  StoreMajorMonotone,
} from "@shopify/polaris-icons";
import api from "../../config/api";
import useDebounce from "../../src/utils/useDebounce";
function pendingEvents(props) {
  const router = useRouter();
  const [organizeEvents, setOrganizeEvents] = useState([]);
  const [toastActive, setToastActive] = useState(false);
  const [queryValue, setQueryValue] = useState(null);
  const [sortValue, setSortValue] = useState("event_date__ASC");
  const [isLoading, setIsLoading] = useState(true);
  const [page, setPage] = useState(0);
  const [hasPrevious, setHasPrevious] = useState(false);
  const [hasNext, setHasNext] = useState(true);

  const debouncedSearchTerm = useDebounce(queryValue, 500);
  const handleOnPrevious = () => {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0;
    router.push(
      "/red_cross/pendingEvents?page=" + (parseInt(page) - 1),
      undefined,
      {
        shallow: true,
      }
    );
  };
  const handleOnNext = () => {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0;
    router.push(
      "/red_cross/pendingEvents?page=" + (parseInt(page) + 1),
      undefined,
      {
        shallow: true,
      }
    );
  };
  const handleSortValueChange = useCallback((value) => setSortValue(value), []);

  const handleQueryValueChange = useCallback(
    (value) => setQueryValue(value),
    []
  );
  const handleQueryValueRemove = useCallback(() => setQueryValue(null), []);
  const handleClearAll = useCallback(() => {
    handleQueryValueRemove();
  }, [handleQueryValueRemove]);

  const filterControl = (
    <Filters
      filters={[]}
      appliedFilters={[]}
      queryValue={queryValue}
      onQueryChange={handleQueryValueChange}
      onQueryClear={handleQueryValueRemove}
      onClearAll={handleClearAll}
    />
  );
  const sortOptions = [
    { label: "Newest Event Date", value: "event_date__DESC" },
    { label: "Oldest Event Date", value: "event_date__ASC" },
  ];

  const getPendingBloodEvent = useCallback(
    (searchFilters, sortFilter, page) => {
      const params1 = searchFilters ? "?name=" + searchFilters : "";
      const params3 = sortFilter
        ? searchFilters
          ? "&order_by=" + sortFilter
          : "?order_by=" + sortFilter
        : "";
      const params = params1 + params3 + "&limit=5&page=" + page;
      api.getPendingBloodEvent(null, params, (isSuccess, response, error) => {
        if (isSuccess) {
          setIsLoading(false);
          setOrganizeEvents(response.data.data);
        } else console.log(error.response);
      });
    },
    [organizeEvents]
  );
  useEffect(() => {
    console.log("tagged", sortValue);
    if (sortValue) {
      setIsLoading(true);
      // Fire off our API call
      getPendingBloodEvent(debouncedSearchTerm, sortValue, page);
    }
  }, [debouncedSearchTerm, sortValue]);

  useEffect(() => {
    if (router.query.page) {
      if (parseInt(router.query.page) == 0) {
        setHasPrevious(false);
      } else if (parseInt(router.query.page) > 0) setHasPrevious(true);
      if (parseInt(router.query.page) == 12) setHasNext(false);
      else if (parseInt(router.query.page) < 12) setHasNext(true);
      setPage(router.query.page);
      setIsLoading(true);
      // Fire off our API call
      getPendingBloodEvent(debouncedSearchTerm, sortValue, router.query.page);
    }
  }, [router.query.page, debouncedSearchTerm]);

  useEffect(() => {
    //COMPONENT DID MOUNT
    // Make sure we have a value (user has entered something in input)
    if (debouncedSearchTerm) {
      setHasPrevious(false);
      // Set isSearching state
      console.log("search", debouncedSearchTerm);
      router.push("/red_cross/pendingEvents?page=0", undefined, {
        shallow: true,
      });
    } else getPendingBloodEvent(null, sortValue, page);
  }, [debouncedSearchTerm]);
  // useEffect(() => {
  //   //COMPONENT DID MOUNT
  //   getPendingBloodEvent();
  // }, [organizeEvents]);
  const showToast =
    toastActive === 1 ? (
      <Frame>
        <Toast
          content="Request Event Accepted"
          onDismiss={() => setToastActive(0)}
        ></Toast>
      </Frame>
    ) : toastActive === 2 ? (
      <Frame>
        <Toast
          content="Request Event Rejected"
          onDismiss={() => setToastActive(0)}
        ></Toast>
      </Frame>
    ) : null;
  const onClickAcceptEvent = useCallback(
    (eventId) => {
      console.log("test", eventId);
      api.postAcceptBloodEvent(null, eventId, (isSuccess, response, error) => {
        //IF SUCCESS Delete the accepted event from the pending list
        if (isSuccess) {
          setOrganizeEvents([
            ...organizeEvents.filter((item) => item.event_id !== eventId),
          ]);
          setToastActive(1);
        } else console.log(error.response);
      });
    },
    [organizeEvents]
  );
  const onClickRejectEvent = useCallback(
    (eventId) => {
      api.postRejectBloodEvent(null, eventId, (isSuccess, response, error) => {
        //IF SUCCESS Delete the rejected event from the pending list
        if (isSuccess) {
          setOrganizeEvents(
            organizeEvents.filter((item) => item.event_id !== eventId)
          );
          setToastActive(2);
        } else console.log(error.response);
      });
    },
    [organizeEvents]
  );
  return (
    <PageLayout
      userName={props.userName}
      userType={props.userType}
      expectedUserType={"red_cross"}
      navigationArray={[
        {
          label: "Donors Management",
          icon: CustomersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/getDonors").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingDonation' to 'red_cross/getDonors'"
              );
            });
          },
        },
        {
          label: "Organizers Management",
          icon: CustomersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/getOrganizers").then(() => {
              console.log(
                "Route successfully from 'red_cross/getDonors' to 'red_cross/getOrganizers'"
              );
            });
          },
        },
        {
          label: "Hospitals Management",
          icon: CustomersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/getHospitals").then(() => {
              console.log(
                "Route successfully from 'red_cross/getDonors' to 'red_cross/getHospitals'"
              );
            });
          },
        },
        {
          label: "Pending Events Requests",
          icon: ActivitiesMajorMonotone,
          onClick: () => {
            //DO NOT THING BECAUSE ALREADY AT THIS ROUTE
          },
        },
        {
          label: " Pending Orders Requests ",
          icon: OrdersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/pendingOrders").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingEvents' to 'red_cross/pendingOrders'"
              );
            });
          },
        },
        {
          label: " Pending Donation ",
          icon: HeartMajorMonotone,
          onClick: () => {
            router.push("/red_cross/pendingDonation").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingEvents' to 'red_cross/pendingDonation'"
              );
            });
          },
        },
        {
          label: "Events Management",
          icon: ActivitiesMajorMonotone,
          onClick: () => {
            router.push("/red_cross/eventsHistory").then(() => {
              console.log(
                "Route successfully from 'red_cross/orderHistory' to 'red_cross/eventsHistorys'"
              );
            });
          },
        },
        {
          label: " Orders Management ",
          icon: OrdersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/orderHistory").then(() => {
              console.log(
                "Route successfully from 'red_cross/eventsHistory' to 'red_cross/ordersHistory'"
              );
            });
          },
        },
        {
          label: " Donation Management",
          icon: HeartMajorMonotone,
          onClick: () => {
            router.push("/red_cross/donationHistory").then(() => {
              console.log(
                "Route successfully from 'red_cross/eventsHIstory' to 'red_cross/donationHistorys'"
              );
            });
          },
        },
        {
          label: " Blood Store ",
          icon: StoreMajorMonotone,
          onClick: () => {
            router.push("/red_cross/bloodstore").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingEvents' to 'red_cross/bloodstore'"
              );
            });
          },
        },
      ]}
    >
      <Page
        title="Pending Events Management"
        subtitle={
          organizeEvents.length
            ? "Event requests process"
            : "No events available"
        }
      >
        <Layout>
          <Layout.Section>
            <Card>
              <ResourceList
                resourceName={{
                  singular: "pending event",
                  plural: "pending events",
                }}
                items={organizeEvents}
                idForItem={(item) => item.event_id}
                filterControl={filterControl}
                loading={isLoading}
                sortValue={sortValue}
                sortOptions={sortOptions}
                onSortChange={handleSortValueChange}
                renderItem={(item) => {
                  const { event_id, name, location, event_date } = item;
                  const media = <Avatar size="medium" name={name} />;
                  const shortcutActions = [
                    {
                      content: "Accept Event",
                      primary: true,
                      onClick: () => onClickAcceptEvent(event_id),
                    },
                    {
                      content: "Reject Event",
                      onClick: () => onClickRejectEvent(event_id),
                    },
                  ];

                  return (
                    <ResourceItem
                      id={event_id}
                      media={media}
                      shortcutActions={shortcutActions}
                    >
                      <h3>
                        <TextStyle variation="strong">{name}</TextStyle>
                      </h3>
                      <div>{location}</div>
                      <div>{timeConverter(event_date)}</div>
                    </ResourceItem>
                  );
                }}
              />
              <div className="outer">
                <div className="inner">
                  <Pagination
                    hasNext={hasNext}
                    hasPrevious={hasPrevious}
                    onPrevious={handleOnPrevious}
                    onNext={handleOnNext}
                  />
                </div>
              </div>
            </Card>
          </Layout.Section>
        </Layout>
        <div style={{ height: "250px" }}>{showToast}</div>
      </Page>
    </PageLayout>
  );
}
export default connect((state) => ({
  userName: state.LoginReducer.userName,
  email: state.LoginReducer.email,
  userType: state.LoginReducer.userType,
  userId: state.LoginReducer.userId,
}))(pendingEvents);
