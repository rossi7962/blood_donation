import React, { useEffect, useState, useCallback } from "react";
import { useRouter } from "next/router";
import { connect } from "react-redux";
import PageLayout from "../../../src/components/PageLayout.js";
import {
  Card,
  Page,
  ResourceList,
  ResourceItem,
  TextStyle,
  Avatar,
  Filters,
  Pagination,
} from "@shopify/polaris";
import {
  OrdersMajorMonotone,
  HeartMajorMonotone,
  CustomersMajorMonotone,
  ActivitiesMajorMonotone,
  StoreMajorMonotone,
  PlusMinor,
} from "@shopify/polaris-icons";
import api from "../../../config/api";
import useDebounce from "../../../src/utils/useDebounce";
function getDonors(props) {
  const router = useRouter();
  const [donors, setDonors] = useState([]);
  const [queryValue, setQueryValue] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [page, setPage] = useState(0);
  const [hasPrevious, setHasPrevious] = useState(false);
  const [hasNext, setHasNext] = useState(true);

  const debouncedSearchTerm = useDebounce(queryValue, 500);
  const handleOnPrevious = () => {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0;
    router.push(
      "/red_cross/getDonors?page=" + (parseInt(page) - 1),
      undefined,
      {
        shallow: true,
      }
    );
  };
  const handleOnNext = () => {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0;
    router.push(
      "/red_cross/getDonors?page=" + (parseInt(page) + 1),
      undefined,
      {
        shallow: true,
      }
    );
  };

  const handleQueryValueChange = useCallback(
    (value) => setQueryValue(value),
    []
  );
  const handleQueryValueRemove = useCallback(() => setQueryValue(null), []);
  const handleClearAll = useCallback(() => {
    handleQueryValueRemove();
  }, [handleQueryValueRemove]);
  const filterControl = (
    <Filters
      queryValue={queryValue}
      filters={[]}
      appliedFilters={[]}
      onQueryChange={handleQueryValueChange}
      onQueryClear={handleQueryValueRemove}
      onClearAll={handleClearAll}
    />
  );

  // const promotedBulkActions = [];

  // const bulkActions = [];
  const getDonors = useCallback(
    (searchFilters, page) => {
      const params1 = searchFilters ? "?name=" + searchFilters : "";
      const params = params1
        ? params1 + "&limit=5&page=" + page
        : params1 + "?limit=5&page=" + page;
      api.getDonors(null, params, (isSuccess, response, error) => {
        if (isSuccess) {
          setIsLoading(false);
          setDonors(response.data.data);
        } else console.log(error.response);
      });
    },
    [donors]
  );

  useEffect(() => {
    if (router.query.page) {
      if (parseInt(router.query.page) == 0) {
        setHasPrevious(false);
      } else if (parseInt(router.query.page) > 0) setHasPrevious(true);
      if (parseInt(router.query.page) == 12) setHasNext(false);
      else if (parseInt(router.query.page) < 12) setHasNext(true);
      setPage(router.query.page);
      setIsLoading(true);
      // Fire off our API call
      getDonors(debouncedSearchTerm, router.query.page);
    }
  }, [router.query.page, debouncedSearchTerm]);

  useEffect(() => {
    //COMPONENT DID MOUNT
    // Make sure we have a value (user has entered something in input)
    if (debouncedSearchTerm) {
      setHasPrevious(false);
      // Set isSearching state
      console.log("search", debouncedSearchTerm);
      router.push("/red_cross/getDonors?page=0", undefined, {
        shallow: true,
      });
    } else getDonors(null, page);
  }, [debouncedSearchTerm]);
  // useEffect(() => {
  //   //COMPONENT DID UPDATE
  //   getDonors();
  // }, [donors]);
  return (
    <PageLayout
      userName={props.userName}
      userType={props.userType}
      expectedUserType={"red_cross"}
      navigationArray={[
        {
          label: "Donors Management",
          icon: CustomersMajorMonotone,
          onClick: () => {},
        },
        {
          label: "Organizers Management",
          icon: CustomersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/getOrganizers").then(() => {
              console.log(
                "Route successfully from 'red_cross/getDonors' to 'red_cross/getOrganizers'"
              );
            });
          },
        },
        {
          label: "Hospitals Management",
          icon: CustomersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/getHospitals").then(() => {
              console.log(
                "Route successfully from 'red_cross/getDonors' to 'red_cross/getHospitals'"
              );
            });
          },
        },
        {
          label: "Pending Events Requests",
          icon: ActivitiesMajorMonotone,
          onClick: () => {
            router.push("/red_cross/pendingEvents").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingDonation' to 'red_cross/pendingEvents'"
              );
            });
          },
        },
        {
          label: " Pending Orders Requests ",
          icon: OrdersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/pendingOrders").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingDonation' to 'red_cross/pendingOrders'"
              );
            });
          },
        },
        {
          label: " Pending Donation ",
          icon: HeartMajorMonotone,
          onClick: () => {
            router.push("/red_cross/pendingDonation").then(() => {
              console.log(
                "Route successfully from 'red_cross/bloodstore' to 'red_cross/pendingDonation'"
              );
            });
          },
        },
        {
          label: "Events Management",
          icon: ActivitiesMajorMonotone,
          onClick: () => {
            router.push("/red_cross/eventsHistory").then(() => {
              console.log(
                "Route successfully from 'red_cross/orderHistory' to 'red_cross/eventsHistorys'"
              );
            });
          },
        },
        {
          label: " Orders Management ",
          icon: OrdersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/orderHistory").then(() => {
              console.log(
                "Route successfully from 'red_cross/eventsHistory' to 'red_cross/ordersHistory'"
              );
            });
          },
        },
        {
          label: " Donation Management",
          icon: HeartMajorMonotone,
          onClick: () => {
            router.push("/red_cross/donationHistory").then(() => {
              console.log(
                "Route successfully from 'red_cross/eventsHIstory' to 'red_cross/donationHistorys'"
              );
            });
          },
        },
        {
          label: " Blood Store ",
          icon: StoreMajorMonotone,
          onClick: () => {
            router.push("/red_cross/bloodstore").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingDonations' to 'red_cross/bloodstore'"
              );
            });
          },
        },
      ]}
    >
      <Page
        title="Donors Management"
        subtitle={donors.length ? "Donors" : "No Donors available"}
      >
        <Card>
          <ResourceList
            resourceName={{ singular: "donor", plural: "donors" }}
            items={donors}
            renderItem={renderItem}
            filterControl={filterControl}
            loading={isLoading}
          />
          <div className="outer">
            <div className="inner">
              <Pagination
                hasNext={hasNext}
                hasPrevious={hasPrevious}
                onPrevious={handleOnPrevious}
                onNext={handleOnNext}
              />
            </div>
          </div>
        </Card>
      </Page>
    </PageLayout>
  );
  function renderItem(item) {
    const { donor_id, blood_type, name, email } = item;
    const shortcutActions = [
      {
        content: "View Detail",
        primary: true,
        onClick: () =>
          router.push(
            "/red_cross/getDonors/[donorId]",
            "/red_cross/getDonors/" + donor_id
          ),
      },
    ];
    const media = <Avatar customer size="medium" name={name} />;
    return (
      <ResourceItem
        id={donor_id}
        shortcutActions={shortcutActions}
        media={media}
      >
        <h3>
          <TextStyle variation="strong">{name}</TextStyle>
        </h3>
        <div>{email}</div>
        <div>
          Blood Type:{"\u00A0".repeat(10)}
          {blood_type}
        </div>
      </ResourceItem>
    );
  }
}
export default connect((state) => ({
  userName: state.LoginReducer.userName,
  email: state.LoginReducer.email,
  userType: state.LoginReducer.userType,
  userId: state.LoginReducer.userId,
}))(getDonors);
