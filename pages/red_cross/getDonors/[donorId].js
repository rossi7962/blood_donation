import React, { useEffect, useState, useCallback } from "react";
import { useRouter } from "next/router";
import { connect } from "react-redux";
import PageLayout from "../../../src/components/PageLayout.js";
import { Card, Page, FormLayout, TextField, Checkbox } from "@shopify/polaris";
import { ArrowLeftMinor } from "@shopify/polaris-icons";
import api from "../../../config/api";
function getDonorsDetail(props) {
  const router = useRouter();
  const [donors, setDonors] = useState(null);
  const { donorId } = router.query;
  // const promotedBulkActions = [];

  // const bulkActions = [];
  const getDonorsDetail = useCallback(
    (donorId) => {
      api.getDonorsDetail(null, donorId, (isSuccess, response, error) => {
        console.log(response.data.data);
        if (isSuccess) setDonors(response.data.data[0]);
        else console.log(error.response);
      });
    },
    [donors]
  );
  useEffect(() => {
    //COMPONENT DID MOUNT
    if (donorId) getDonorsDetail(donorId);
  }, [donorId]);
  return (
    <PageLayout
      userName={props.userName}
      userType={props.userType}
      expectedUserType={"red_cross"}
      navigationArray={[
        {
          label: "Go Back",
          icon: ArrowLeftMinor,
          onClick: () => {
            router.back();
          },
        },
      ]}
    >
      <Page title="Donor Information">
        {donors && (
          <Card>
            <Card.Section title="Profile">
              <FormLayout>
                <TextField
                  label="Email"
                  placeholder="Email"
                  value={donors.email}
                  readOnly
                />
                <TextField
                  label="Username"
                  placeholder="Username"
                  value={donors.name}
                  readOnly
                />
                <TextField
                  label="Address"
                  placeholder="Address"
                  value={donors.address}
                  readOnly
                />
                <TextField
                  label="Blood Type"
                  placeholder="Blood Type"
                  value={donors.blood_type}
                  readOnly
                />
                <TextField
                  label="Date of Birth"
                  placeholder="Date of Birth"
                  value={donors.dob}
                  readOnly
                />
                <TextField
                  label="Gender"
                  placeholder="gender"
                  value={donors.gender}
                  readOnly
                />
                <TextField
                  label="Weight (kg)"
                  type="number"
                  value={donors.weight + ""}
                  readOnly
                />
                <TextField
                  label="Height (cm)"
                  type="number"
                  value={donors.height + ""}
                  readOnly
                />
              </FormLayout>
            </Card.Section>
            <Card.Section title="Donation Requirement">
              <FormLayout>
                Have you had tattoo for the last 3 months?
                <Checkbox label="Yes" checked={donors.tattoo_last_12_month} />
                <Checkbox label="No" checked={!donors.tattoo_last_12_month} />
                Does your blood have high cholesterol rate?
                <Checkbox label="Yes" checked={donors.cholesterol} />
                <Checkbox label="No" checked={!donors.cholesterol} />
                Do you have positive test result with HIV?
                <Checkbox label="Yes" checked={donors.positive_test_HIV} />
                <Checkbox label="No" checked={!donors.positive_test_HIV} />
                Have you ever infected with infectious disease?
                <Checkbox label="Yes" checked={donors.infectious_disease} />
                <Checkbox label="No" checked={!donors.infectious_disease} />
                Do you have cancer?
                <Checkbox label="Yes" checked={donors.cancer} />
                <Checkbox label="No" checked={!donors.cancer} />
              </FormLayout>
            </Card.Section>
          </Card>
        )}
      </Page>
    </PageLayout>
  );
}
export default connect((state) => ({
  userName: state.LoginReducer.userName,
  email: state.LoginReducer.email,
  userType: state.LoginReducer.userType,
  userId: state.LoginReducer.userId,
}))(getDonorsDetail);
