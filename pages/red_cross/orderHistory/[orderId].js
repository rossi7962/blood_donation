import React, { useEffect, useState, useCallback } from "react";
import { useRouter } from "next/router";
import { connect } from "react-redux";
import PageLayout from "../../../src/components/PageLayout.js";
import { Card, Page, Stack, DescriptionList } from "@shopify/polaris";
import { ArrowLeftMinor } from "@shopify/polaris-icons";
import api from "../../../config/api";
import timeConverter from "../../../src/utils/timeConverter.js";
import DonationList from "../../../src/components/DonationList.js";
function getHospitalsDetail(props) {
  const router = useRouter();
  const { orderId } = router.query;
  const [hospitalOrders, setHospitalOrders] = useState([]);
  const [hospitalDonations, setHospitalDonations] = useState([]);
  const getAcceptedOrdersDetail = useCallback(
    (orderId) => {
      api.getAcceptedOrdersDetail(
        null,
        orderId,
        (isSuccess, response, error) => {
          if (isSuccess) {
            if (response.data.data.length === 0) setHospitalOrders(null);
            else setHospitalOrders(response.data.data[0]);
          } else console.log(error.response);
        }
      );
    },
    [hospitalOrders]
  );
  const getDonationList = useCallback(
    (orderId) => {
      api.getDonationList(null, orderId, (isSuccess, response, error) => {
        //IF SUCCESS Delete the accepted order from the pending list
        if (isSuccess) {
          console.log(response.data.data);
          if (response.data.data.length === 0) setHospitalDonations([]);
          else setHospitalDonations(response.data.data);
        } else {
          console.log(error.response);
          setError(error.response.data.err);
          setToastActive(3);
        }
      });
    },
    [hospitalOrders]
  );
  useEffect(() => {
    //COMPONENT DID MOUNT
    if (orderId) {
      getAcceptedOrdersDetail(orderId);
      getDonationList(orderId);
    }
  }, [orderId]);
  return (
    <PageLayout
      userName={props.userName}
      userType={props.userType}
      expectedUserType={"red_cross"}
      navigationArray={[
        {
          label: "Go Back",
          icon: ArrowLeftMinor,
          onClick: () => {
            router.back();
          },
        },
      ]}
    >
      <Page title="Order Receipt">
        <Card>
          <Stack vertical>
            <Card.Section title="Order information">
              {hospitalOrders && (
                <DescriptionList
                  items={[
                    {
                      term: "Hospital",
                      description: hospitalOrders.hospital_name,
                    },
                    { term: "Order id", description: hospitalOrders.order_id },
                    {
                      term: "Blood Type",
                      description: hospitalOrders.blood_type,
                    },
                    {
                      term: "Amount",
                      description: hospitalOrders.amount,
                    },
                    {
                      term: "Order Date",
                      description: timeConverter(hospitalOrders.order_date),
                    },
                  ]}
                />
              )}
            </Card.Section>
            <Card.Section title="Donations ID List">
              <DonationList data={hospitalDonations} router={router} />
            </Card.Section>
          </Stack>
        </Card>
      </Page>
    </PageLayout>
  );
}
export default connect((state) => ({
  userName: state.LoginReducer.userName,
  email: state.LoginReducer.email,
  userType: state.LoginReducer.userType,
  userId: state.LoginReducer.userId,
}))(getHospitalsDetail);
