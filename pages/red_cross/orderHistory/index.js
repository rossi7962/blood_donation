import React, { useEffect, useState, useCallback } from "react";
import { useRouter } from "next/router";
//import Router from "next/router";
import { connect } from "react-redux";
import timeConverter from "../../../src/utils/timeConverter";
import PageLayout from "../../../src/components/PageLayout.js";
import {
  Card,
  Avatar,
  DescriptionList,
  Page,
  ChoiceList,
  ResourceList,
  Filters,
  Pagination,
  ResourceItem,
  TextStyle,
  Layout,
} from "@shopify/polaris";
import {
  CustomersMajorMonotone,
  OrdersMajorMonotone,
  HeartMajorMonotone,
  ActivitiesMajorMonotone,
  StoreMajorMonotone,
} from "@shopify/polaris-icons";
import api from "../../../config/api";
import useDebounce from "../../../src/utils/useDebounce";
function orderHistory(props) {
  const router = useRouter();
  const [hospitalOrders, setHospitalOrders] = useState([]);
  const [taggedStatus, setTaggedStatus] = useState(null);
  const [queryValue, setQueryValue] = useState(null);
  const [sortValue, setSortValue] = useState("order_date__ASC");
  const [isLoading, setIsLoading] = useState(true);
  const [page, setPage] = useState(0);
  const [hasPrevious, setHasPrevious] = useState(false);
  const [hasNext, setHasNext] = useState(true);

  const debouncedSearchTerm = useDebounce(queryValue, 500);
  const handleOnPrevious = () => {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0;
    router.push(
      "/red_cross/orderHistory?page=" + (parseInt(page) - 1),
      undefined,
      {
        shallow: true,
      }
    );
  };
  const handleOnNext = () => {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0;
    router.push(
      "/red_cross/orderHistory?page=" + (parseInt(page) + 1),
      undefined,
      {
        shallow: true,
      }
    );
  };

  const handleSortValueChange = useCallback((value) => setSortValue(value), []);

  const handleTaggedStatusChange = useCallback(
    (value) => setTaggedStatus(value),
    []
  );
  const handleQueryValueChange = useCallback(
    (value) => setQueryValue(value),
    []
  );
  const handleTaggedStatusRemove = useCallback(() => setTaggedStatus(null), []);
  const handleQueryValueRemove = useCallback(() => setQueryValue(null), []);
  const handleClearAll = useCallback(() => {
    handleTaggedStatusRemove();
    handleQueryValueRemove();
  }, [handleQueryValueRemove, handleTaggedStatusRemove]);
  const filters = [
    {
      key: "Order Status",
      label: "Order status",
      filter: (
        <ChoiceList
          title="Order status"
          titleHidden
          choices={[
            { label: "approved", value: "approved" },
            { label: "rejected", value: "rejected" },
          ]}
          selected={taggedStatus || []}
          onChange={handleTaggedStatusChange}
          allowMultiple
        />
      ),
      shortcut: true,
    },
  ];
  const appliedFilters = [];
  if (!isEmpty(taggedStatus)) {
    const key = "taggedStatus";
    appliedFilters.push({
      key,
      label: disambiguateLabel(key, taggedStatus),
      onRemove: handleTaggedStatusRemove,
    });
  }
  const filterControl = (
    <Filters
      queryValue={queryValue}
      filters={filters}
      appliedFilters={appliedFilters}
      onQueryChange={handleQueryValueChange}
      onQueryClear={handleQueryValueRemove}
      onClearAll={handleClearAll}
    />
  );
  const sortOptions = [
    { label: "Newest Order Date", value: "order_date__DESC" },
    { label: "Oldest Order Date", value: "order_date__ASC" },
  ];
  const getAcceptedOrders = useCallback(
    (searchFilters, statusFilters, sortFilter, page) => {
      const params1 = searchFilters ? "?name=" + searchFilters : "";
      const params2 = statusFilters
        ? searchFilters
          ? "&status=" + statusFilters
          : "?status=" + statusFilters
        : "";
      const params3 = sortFilter
        ? statusFilters
          ? "&order_by=" + sortFilter
          : searchFilters
          ? "&order_by=" + sortFilter
          : "?order_by=" + sortFilter
        : "";
      const params = params1 + params2 + params3 + "&limit=5&page=" + page;
      api.getAcceptedOrders(null, params, (isSuccess, response, error) => {
        if (isSuccess) {
          setIsLoading(false);
          setHospitalOrders(response.data.data);
        } else console.log(error.response);
      });
    },
    [hospitalOrders]
  );
  useEffect(() => {
    console.log("tagged", taggedStatus, sortValue);
    if (taggedStatus || sortValue) {
      setIsLoading(true);
      // Fire off our API call
      if (parseInt(page) == 0) {
        getAcceptedOrders(debouncedSearchTerm, taggedStatus, sortValue, page);
      } else
        router.push("/red_cross/orderHistory?page=0", undefined, {
          shallow: true,
        });
    }
  }, [debouncedSearchTerm, taggedStatus, sortValue]);
  useEffect(() => {
    console.log(router.query.page);
    if (router.query.page) {
      if (parseInt(router.query.page) == 0) {
        setHasPrevious(false);
      } else if (parseInt(router.query.page) > 0) setHasPrevious(true);
      if (parseInt(router.query.page) == 12) setHasNext(false);
      else if (parseInt(router.query.page) < 12) setHasNext(true);
      setPage(router.query.page);
      setIsLoading(true);
      // Fire off our API call
      getAcceptedOrders(
        debouncedSearchTerm,
        taggedStatus,
        sortValue,
        router.query.page
      );
    }
  }, [router.query.page, debouncedSearchTerm]);

  useEffect(() => {
    //COMPONENT DID MOUNT
    // Make sure we have a value (user has entered something in input)
    if (debouncedSearchTerm) {
      setHasPrevious(false);
      // Set isSearching state
      console.log("search", debouncedSearchTerm);
      router.push("/red_cross/orderHistory?page=0", undefined, {
        shallow: true,
      });
    } else getAcceptedOrders(null, taggedStatus, sortValue, page);
  }, [debouncedSearchTerm]);
  return (
    <PageLayout
      userName={props.userName}
      userType={props.userType}
      expectedUserType={"red_cross"}
      navigationArray={[
        {
          label: "Donors Management",
          icon: CustomersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/getDonors").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingDonation' to 'red_cross/getDonors'"
              );
            });
          },
        },
        {
          label: "Organizers Management",
          icon: CustomersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/getOrganizers").then(() => {
              console.log(
                "Route successfully from 'red_cross/getDonors' to 'red_cross/getOrganizers'"
              );
            });
          },
        },
        {
          label: "Hospitals Management",
          icon: CustomersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/getHospitals").then(() => {
              console.log(
                "Route successfully from 'red_cross/getDonors' to 'red_cross/getHospitals'"
              );
            });
          },
        },
        {
          label: "Pending Events Requests",
          icon: ActivitiesMajorMonotone,
          onClick: () => {
            router.push("/red_cross/pendingEvents").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingorders' to 'red_cross/pendingEvents'"
              );
            });
          },
        },
        {
          label: " Pending Orders Requests ",
          icon: OrdersMajorMonotone,
          onClick: () => {
            router.push("/red_cross/pendingOrders").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingEvents' to 'red_cross/pendingOrders'"
              );
            });
          },
        },
        {
          label: " Pending Donation ",
          icon: HeartMajorMonotone,
          onClick: () => {
            router.push("/red_cross/pendingDonation").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingorders' to 'red_cross/pendingDonation'"
              );
            });
          },
        },
        {
          label: "Events Management",
          icon: ActivitiesMajorMonotone,
          onClick: () => {
            router.push("/red_cross/eventsHistory").then(() => {
              console.log(
                "Route successfully from 'red_cross/orderHistory' to 'red_cross/eventsHistorys'"
              );
            });
          },
        },
        {
          label: " Orders Management ",
          icon: OrdersMajorMonotone,
          onClick: () => {},
        },
        {
          label: " Donation Management",
          icon: HeartMajorMonotone,
          onClick: () => {
            router.push("/red_cross/donationHistory").then(() => {
              console.log(
                "Route successfully from 'red_cross/orderHIstory' to 'red_cross/donationHistorys'"
              );
            });
          },
        },
        {
          label: " Blood Store ",
          icon: StoreMajorMonotone,
          onClick: () => {
            router.push("/red_cross/bloodstore").then(() => {
              console.log(
                "Route successfully from 'red_cross/pendingorders' to 'red_cross/bloodstore'"
              );
            });
          },
        },
      ]}
    >
      <Page
        title="Orders Management"
        subtitle={
          hospitalOrders.length ? "order history" : "No orders available"
        }
      >
        <Layout>
          <Layout.Section>
            <Card>
              <ResourceList
                resourceName={{
                  singular: "order",
                  plural: "orders by hospital name",
                }}
                items={hospitalOrders}
                idForItem={(item) => item.order_id}
                renderItem={renderItem}
                filterControl={filterControl}
                loading={isLoading}
                sortValue={sortValue}
                sortOptions={sortOptions}
                onSortChange={handleSortValueChange}
              />
              <div className="outer">
                <div className="inner">
                  <Pagination
                    hasNext={hasNext}
                    hasPrevious={hasPrevious}
                    onPrevious={handleOnPrevious}
                    onNext={handleOnNext}
                  />
                </div>
              </div>
            </Card>
          </Layout.Section>
        </Layout>
      </Page>
    </PageLayout>
  );
  function renderItem(item) {
    const {
      order_id,
      blood_type,
      amount,
      hospital_name,
      order_date,
      status,
    } = item;
    const media = <Avatar size="medium" name={hospital_name} />;
    const shortcutActions =
      status === "approved"
        ? [
            {
              content: "View Order Receipt",
              primary: true,
              onClick: () =>
                router.push(
                  "/red_cross/orderHistory/[orderId]",
                  "/red_cross/orderHistory/" + order_id
                ),
            },
          ]
        : null;
    const dlItems = [
      {
        term: "Blood Type",
        description: blood_type,
      },
      {
        term: "Amount",
        description: amount + "mL",
      },
      {
        term: "Order Date",
        description: timeConverter(order_date),
      },
    ];
    return (
      <ResourceItem
        id={order_id}
        media={media}
        shortcutActions={shortcutActions}
      >
        <h3>
          <TextStyle variation="strong">{hospital_name}</TextStyle>
        </h3>
        <h4>
          <TextStyle
            variation={status === "approved" ? "positive" : "negative"}
          >
            {status}
          </TextStyle>
        </h4>
        <DescriptionList items={dlItems} />
      </ResourceItem>
    );
  }
}
function disambiguateLabel(key, value) {
  switch (key) {
    case "taggedWith":
      return `Tagged with ${value}`;
    case "taggedStatus":
      return value.map((val) => `Event ${val}`).join(", ");
    default:
      return value;
  }
}
function isEmpty(value) {
  if (Array.isArray(value)) {
    return value.length === 0;
  } else {
    return value === "" || value == null;
  }
}
export default connect((state) => ({
  userName: state.LoginReducer.userName,
  email: state.LoginReducer.email,
  userType: state.LoginReducer.userType,
  userId: state.LoginReducer.userId,
}))(orderHistory);
