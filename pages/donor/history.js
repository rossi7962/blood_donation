import React, {useEffect, useState} from "react";
import Router from "next/router";
import {connect} from 'react-redux';
import PageLayout from "../../src/components/PageLayout.js"
import {Card, Layout, Page, EmptyState, SkeletonBodyText} from "@shopify/polaris"
import {CalendarTickMajorMonotone, HeartMajorMonotone} from "@shopify/polaris-icons";
import api from "../../config/api";

function History(props) {
    const [loadingDefaultPageData, setLoadingDefaultPageData] = useState(true);
    const [registeredEvents, setRegisteredEvents] = useState([]);

    useEffect(() => {
        // COMPONENT DID UPDATE

        // IF GET USER DATA SUCCESSFULLY THEN GET RUN THESE FUNCTIONS
        if (props.userName) api.getRegisteredBloodEvent((isSuccess, response, error) => {
            setLoadingDefaultPageData(false);
            if (isSuccess) {
                console.log(response);
                setRegisteredEvents(response.data.data);
            } else console.log(error.response);
        });

    }, [props.userName]);

    const autoAddZeroInFront = (number) => {
        return number > 9 ? number : "0" + number;
    };

    const generateDateStringFromStamp = (unixTimeStampValue) => {
        let d = new Date(unixTimeStampValue * 1000);
        return (
            autoAddZeroInFront(d.getHours()) + ":" +
            autoAddZeroInFront(d.getMinutes()) + ":" +
            autoAddZeroInFront(d.getSeconds()) + "   " +
            autoAddZeroInFront(d.getDate()) + "/" +
            autoAddZeroInFront(d.getMonth()) + "/" +
            autoAddZeroInFront(d.getFullYear())
        );
    };

    return (
        <PageLayout userName={props.userName}
                    userType={props.userType}
                    expectedUserType={"donor"}
                    navigationArray={[
                        {
                            label: 'Donate Blood',
                            icon: HeartMajorMonotone,
                            onClick: () => {
                                Router.push("/donor/event").then(() => {
                                    console.log("Route successfully from 'donor/FormList' to 'donor/event'");
                                })
                            },
                        },
                        {
                            label: 'Registered Blood Event',
                            icon: CalendarTickMajorMonotone,
                            onClick: () => {
                                //DO NOT THING BECAUSE ALREADY AT THIS ROUTE
                            },
                        },
                    ]}
                    pageTitle={"Registered Events - Blood Donation"}

        >

            <Page title="Registered Blood Event"
                  subtitle="This page contains data on registered blood event"
            >
                {loadingDefaultPageData ? <SkeletonBodyText/> : registeredEvents.length ?
                    <Layout>
                        {registeredEvents.map((item) => {
                            return (
                                <Layout.Section key={item.event_id}>
                                    <Card>
                                        <Card.Header title={item.name}/>
                                        <Card.Section>
                                            <p>Blood Identifier: {item.blood_id}</p>
                                        </Card.Section>
                                        <Card.Section>
                                            <p>Donated Date: {generateDateStringFromStamp(item.donate_date)} </p>
                                            <p>Event Date: {generateDateStringFromStamp(item.event_date)}</p>
                                            <p>Location: {item.location}</p><br/>
                                            <p>
                                                <strong>{item.status === "pending" ? "Status: Pending" : "Status: Approved"}</strong>
                                            </p>
                                        </Card.Section>
                                    </Card>
                                </Layout.Section>
                            )
                        })}
                    </Layout> : <EmptyState
                        centeredLayout
                        heading="No blood donation in your history"
                        action={{content: 'Request blood donation now!', onAction: () => { Router.push("/donor/event")}}}
                        image="https://cdn.shopify.com/s/files/1/2376/3301/products/emptystate-files.png"
                    >
                        <p>
                            Remember to add required information to your profile before start making blood donation requests
                        </p>
                    </EmptyState>
                }
            </Page>
        </PageLayout>
    );
}

export default connect(state => ({
    userName: state.LoginReducer.userName,
    email: state.LoginReducer.email,
    userType: state.LoginReducer.userType,
    userId: state.LoginReducer.userId
}))(History);

