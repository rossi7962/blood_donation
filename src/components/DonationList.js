import React, { useCallback, useState } from "react";
import {
  ResourceItem,
  ResourceList,
  TextStyle,
  Avatar,
  DescriptionList,
  Pagination,
} from "@shopify/polaris";
export default function DonationList(props) {
  function renderItem(item) {
    const {
      blood_id,
      blood_type,
      amount,
      donor_name,
      event_name,
      hospital_name,
      donor_id,
      event_id,
      order_id,
      hospital_id,
      status,
    } = item;
    const shortcutActions = [
      {
        content: "View Donor",
        primary: true,
        onClick: () =>
          props.router.push(
            "/red_cross/getDonors/[donorId]",
            "/red_cross/getDonors/" + donor_id
          ),
      },
      {
        content: "View Event",
        onClick: () =>
          props.router.push(
            "/red_cross/eventsHistory/[eventId]",
            "/red_cross/eventsHistory/" + event_id
          ),
      },
    ];
    if (hospital_id)
      shortcutActions.push({
        content: "View Hospital",
        onClick: () =>
          props.router.push(
            "/red_cross/getHospitals/[hospitalId]",
            "/red_cross/getHospitals/" + hospital_id
          ),
      });
    if (order_id)
      shortcutActions.push({
        content: "View Order Receipt",
        onClick: () =>
          props.router.push(
            "/red_cross/orderHistory/[orderId]",
            "/red_cross/orderHistory/" + order_id
          ),
      });
    const media = <Avatar customer size="medium" name={donor_name} />;
    const dlItems = [
      {
        term: "Blood Type",
        description: blood_type,
      },
      {
        term: "Amount",
        description: amount + "mL",
      },
      {
        term: "Event",
        description: event_name,
      },
      {
        term: "Donor",
        description: donor_name,
      },
    ];
    if (hospital_name)
      dlItems.push({
        term: "Hospital",
        description: hospital_name,
      });
    return (
      <ResourceItem
        id={blood_id}
        media={media}
        shortcutActions={shortcutActions}
      >
        <h3>
          <TextStyle variation="strong">Blood Identifier: {blood_id}</TextStyle>
        </h3>
        <h4>
          <TextStyle
            variation={status === "rejected" ? "negative" : "positive"}
          >
            {status}
          </TextStyle>
        </h4>
        <DescriptionList items={dlItems} />
      </ResourceItem>
    );
  }
  return (
    <>
      <ResourceList
        resourceName={{
          singular: "donation",
          plural: "donations by donor name",
        }}
        items={props.data}
        idForItem={(item) => item.blood_id}
        renderItem={renderItem}
        loading={props.isLoading || false}
        filterControl={props.filterControl || null}
        sortValue={props.sortValue || null}
        sortOptions={props.sortOptions || null}
        onSortChange={props.onSortChange || null}
      />
      <div className="outer">
        <div className="inner">
          <Pagination
            hasPrevious={props.hasPrevious || false}
            hasNext={props.hasNext || false}
            onNext={props.onNext}
            onPrevious={props.onPrevious}
          />
        </div>
      </div>
    </>
  );
}
