const DonorUtils = {
    checkRequiredDonateAdditionalInformation: (data) => {
        if (data.height === null ||
            data.weight === null ||
            data.gender === null ||

            data.blood_type === null ||
            data.tattoo_last_12_month === null ||
            data.cholesterol === null ||
            data.positive_test_HIV === null ||
            data.infectious_disease === null ||
            data.cancer === null ) return false;
        else return true;
    },
};

export default DonorUtils;